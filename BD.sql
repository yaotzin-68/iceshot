/*
 Navicat Premium Data Transfer

 Source Server         : Panditas
 Source Server Type    : MySQL
 Source Server Version : 50173
 Source Host           : boletopanditas.com
 Source Database       : db_iceshot

 Target Server Type    : MySQL
 Target Server Version : 50173
 File Encoding         : utf-8

 Date: 05/22/2014 14:00:21 PM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `core_cloudfiles`
-- ----------------------------
DROP TABLE IF EXISTS `core_cloudfiles`;
CREATE TABLE `core_cloudfiles` (
  `core_cloudfiles_id` int(11) NOT NULL AUTO_INCREMENT,
  `core_cloudfiles_filename` varchar(255) CHARACTER SET utf8 NOT NULL,
  `core_cloudfiles_size` float NOT NULL,
  `core_cloudfiles_extension` varchar(10) CHARACTER SET utf8 NOT NULL,
  `core_cloudfiles_content` mediumblob NOT NULL,
  `core_cloudfiles_content_type` varchar(20) CHARACTER SET utf8 NOT NULL,
  `core_cloudfiles_notas` varchar(512) CHARACTER SET utf8 DEFAULT NULL,
  `core_cloudfiles_autor` varchar(100) CHARACTER SET utf8 NOT NULL,
  `core_cloudfiles_autor_sessid` varchar(100) DEFAULT NULL,
  `core_cloudfiles_editor` varchar(100) CHARACTER SET utf8 NOT NULL,
  `core_cloudfiles_editor_sessid` varchar(100) DEFAULT NULL,
  `core_cloudfiles_fecha_creacion` datetime NOT NULL,
  `core_cloudfiles_fecha_modificacion` datetime NOT NULL,
  PRIMARY KEY (`core_cloudfiles_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
--  Table structure for `iceshot_actividades`
-- ----------------------------
DROP TABLE IF EXISTS `iceshot_actividades`;
CREATE TABLE `iceshot_actividades` (
  `iceshot_actividades_id` int(11) NOT NULL AUTO_INCREMENT,
  `iceshot_actividades_id_facebook` bigint(20) DEFAULT NULL,
  `iceshot_actividades_actividad` varchar(50) DEFAULT NULL,
  `iceshot_actividades_session` varchar(250) DEFAULT NULL,
  `iceshot_actividades_fecha_creacion` datetime DEFAULT NULL,
  `iceshot_actividades_fecha_modificacion` datetime DEFAULT NULL,
  PRIMARY KEY (`iceshot_actividades_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `iceshot_useragent`
-- ----------------------------
DROP TABLE IF EXISTS `iceshot_useragent`;
CREATE TABLE `iceshot_useragent` (
  `iceshot_useragent_id` int(11) NOT NULL AUTO_INCREMENT,
  `iceshot_useragent_id_facebook` bigint(20) DEFAULT NULL,
  `iceshot_useragent_useragent` varchar(250) DEFAULT NULL,
  `iceshot_useragent_fecha_creacion` datetime DEFAULT NULL,
  `iceshot_useragent_fecha_modificacion` datetime DEFAULT NULL,
  PRIMARY KEY (`iceshot_useragent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `iceshot_userfb`
-- ----------------------------
DROP TABLE IF EXISTS `iceshot_userfb`;
CREATE TABLE `iceshot_userfb` (
  `iceshot_userfb_id` int(11) NOT NULL AUTO_INCREMENT,
  `iceshot_userfb_id_fb` bigint(20) DEFAULT NULL,
  `iceshot_userfb_birthday` date DEFAULT NULL,
  `iceshot_userfb_email` varchar(100) DEFAULT NULL,
  `iceshot_userfb_name` varchar(100) DEFAULT NULL,
  `iceshot_userfb_gender` varchar(10) DEFAULT NULL,
  `iceshot_userfb_hometown` varchar(100) DEFAULT NULL,
  `iceshot_userfb_fecha_creacion` datetime DEFAULT NULL,
  `iceshot_userfb_fecha_modificacion` datetime DEFAULT NULL,
  PRIMARY KEY (`iceshot_userfb_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

SET FOREIGN_KEY_CHECKS = 1;
