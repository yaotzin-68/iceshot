var DEBUG  	= false;

$(document).ready(function()
{
	if(DEBUG) console.log('...DOM ready!');
	
	App.init();
});

function consoleLog(mensaje)
{
	if(DEBUG)
		console.log(mensaje);
}

/**
 * Declaración de la App
 * http://www.facebook.com/dialog/pagetab?app_id=YOUR_APP_ID&next=YOUR_URL
 */
var App = new function()
{
	this.intfc		= 'assets/php/intfc.php';
	this.userID 		= '';
	this.accessToken 	= '';
	this.canvasSize		= 1228;
	this.appId		= '';
	this.FB;
	this.fbPermissions	= '';
	this.videos 		= new Array();
	this.videosIndice	= 0;
	this.youtubePlayer;
	this.youtubeDone	= false;
	this.videoHash		= '';
	
	/**
	 * Inicializa la Aplicación
	 */
	this.init = function()
	{
		if(DEBUG) console.log('...App.init()');
		
		
		
		this._attachActionEvents();
		
		var posting = $.post(App.intfc,
		{
			action: 'getFBID',
			accessToken: 'getFBID'
		});
		
		posting.done(function(data)
		{
			data = data.split('#')
			
			App.appId  = data[0];
			App.fbPermissions = data[1]
			
			App.validarUsuarioFB();
		});
		
		
	};
	
	this.decode_base64 = function(s)
	{
	var e={},i,k,v=[],r='',w=String.fromCharCode;
	var n=[[65,91],[97,123],[48,58],[43,44],[47,48]];
    
	for(z in n){for(i=n[z][0];i<n[z][1];i++){v.push(w(i));}}
	for(i=0;i<64;i++){e[v[i]]=i;}
    
	for(i=0;i<s.length;i+=72){
	var b=0,c,x,l=0,o=s.substring(i,i+72);
	     for(x=0;x<o.length;x++){
		    c=e[o.charAt(x)];b=(b<<6)+c;l+=6;
		    while(l>=8){r+=w((b>>>(l-=8))%256);}
	     }
	}
	return r;
	}
	
	/*
	 * Valida si el usuario de FB ya autorizo la app sino instala la app
	 */
	this.validarUsuarioFB = function()
	{
		$.ajaxSetup({ cache: true });
		$.getScript('//connect.facebook.net/es_LA/all.js', function()
		{
			FB.init(
			{
				appId: App.appId,
				status : true, // check login status
				cookie : true, // enable cookies to allow the server to access the session
				xfbml : true, // parse XFBML
				oauth: true
			});
			
			FB.getLoginStatus(function(response)
			{
				consoleLog(response.status);

				// Here we subscribe to the auth.authResponseChange JavaScript event. This event is fired
				// for any authentication related change, such as login, logout or session refresh. This means that
				// whenever someone who was previously logged out tries to log in again, the correct case below 
				// will be handled. 
				if (response.status === 'connected')
				{
					// the user is logged in and has authenticated your
					// app, and response.authResponse supplies
					// the user's ID, a valid access token, a signed
					// request, and the time the access token 
					// and signed request each expire
					
					App.userID 	  = response.authResponse.userID;
					App.accessToken   = response.authResponse.accessToken;
					App.signedRequest = response.authResponse.signedRequest;
					
					consoleLog('App.userID '+App.userID);
					consoleLog('App.accessToken '+App.accessToken);
					//console.log('App.signedRequest '+response.authResponse.signedRequest);
					
					//var data = App.signedRequest.split('.')[1];
					//data = JSON.parse(App.decode_base64(data));
					
					
					/*if (App._GetURLParameter('app_data')=='REDIRECT')
					{
						window.top.location.href = "https://www.facebook.com/TiaRosaMx/app_1445312665712600";
					}*/
					

					
					
					App.inicio();
					
					
					
				}
				else if (response.status === 'not_authorized')
				{
					// the user is logged in to Facebook, 
					// but has not authenticated your app
					consoleLog('no autorizado');
					
					FB.login(function(response) 
					{
						if (response.authResponse) 
						{
							App.userID 	= response.authResponse.userID;
							App.accessToken = response.authResponse.accessToken;
							
							consoleLog('App.userID '+App.userID);
							consoleLog('App.accessToken '+App.accessToken);
							
							App.inicio();
   						}
   						else
   						{
							window.location = "assets/pages/noAutorizado.html";
   						}
					},
					{
						scope: App.fbPermissions
					});
				}
				else
				{
					// the user isn't logged in to Facebook.
					consoleLog('no loggeado a FB');
					FB.login();
				}
				
			});
			
			FB.Canvas.setSize({ height: App.canvasSize });
			App.FB = FB;
		});
	}
	
	/*
	 * Guarda los datos del usuario de FB
	 */
	this.saveUserFB = function()
	{
		App.FB.api('/me', function(response)
		{
			
			//var arr = $.map(response, function(el) { return el; });
			//console.log(arr);
			
			var posting = $.post(App.intfc,
			{
				action: 'saveUserFB',
				accessToken: App.accessToken,
				data: response
				
			});
		});
		
		

	}
	
	/**
	 * Direcciona a la sección inicial de la app (Pantalla 1)
	 */
	this.inicio = function()
	{
		//App.setAccessToken();
		
		App.FB.Canvas.scrollTo(0,0);
		App.FB.Canvas.setSize({ height: App.canvasSize });

		var posting = $.post(App.intfc,
		{
		    action: 'saveUseragent',
		    accessToken: App.accessToken,
		    userID:	App.userID
		});
		
		App.saveUserFB();
		
		$('#dynamic').animate(
		{
			left: 1000
		},1000);
		
		App.videoHash = App._GetURLParameter('app_data');
		
		var posting = $.post(App.intfc,
		{
			action: 'getHome',
			accessToken: this.accessToken
		});
		
		posting.done(function(data)
		{
			App.FB.Canvas.setAutoGrow();
			$('#dynamic').html(data);
			
			$('.dynamic').imagesLoaded()
			.always( function( instance )
			{
				$('#dynamic').animate(
				{
					left: 0
				},1000);
				
				App.startPlayers();
				
				App._attachActionEvents();
				
				
			});
			
			
		});
	};
	
	this.startPlayers = function()
	{
		var posting = $.post(App.intfc,
		{
			action: 'getVideos',
			accessToken: this.accessToken
		});
		
		posting.done(function(data)
		{
			consoleLog(data);
			App.videos = data.split(',');
			/*App.videos.push('Mzf9XO7mvEI');
			App.videos.push('rUqCrc4tUhM');
			App.videos.push('q8-aoYR-bYo');
			App.videos.push('C6SEwTyJtpE');
			App.videos.push('f04zDTlSt4I');*/
			
			var players = '';
			for(i=0;i<App.videos.length;i++)
			{
				players += '<div id="player'+i+'" class="youtubeVideo" data-action="showVideo" data-param="'+App.videos[i]+'">';
				//players += '	<div class="carruselLogo"></div>';
				players += '	<div class="carruselPlay"></div>';
				players += '	<div class="video">';
				players += '		<img src="https://img.youtube.com/vi/'+App.videos[i]+'/maxresdefault.jpg">';
				players += '	</div>';
				players += '</div>';
			}
			
			$('.youtubeCarruselMov').html(players);
		
			App.startPlayer(App.videos[App.videosIndice]);
			App._attachActionEvents();
		});
		
		
		
		
	}
	
	this.carruselNext = function()
	{
		$('#player').hide();
		$('#player').html('');
		$('.youtubeVideo:first').appendTo($('.youtubeCarruselMov'));
		
		//consoleLog(App.videosIndice+ ' < '+(App.videos.length-1));
		if (App.videosIndice < (App.videos.length-1))
		{
			App.videosIndice++;
		}
		else
			App.videosIndice=0;
		
		
		App.startPlayer(App.videos[App.videosIndice]);
	}
	
	this.carruselPrev = function()
	{
		$('#player').hide();
		$('#player').html('');
		$('.youtubeVideo:last').prependTo($('.youtubeCarruselMov'));
		
		if (App.videosIndice > 0)
		{
			App.videosIndice--;
		}
		else
			App.videosIndice=(App.videos.length-1);
			
		App.startPlayer(App.videos[App.videosIndice]);
	}
	
	this.getComentarios = function(id)
	{
		consoleLog('getComentarios '+id);
		consoleLog(App.userID);
		$('#comentarios').html('Cargando comentarios...');
		
		var posting = $.post(App.intfc,
		{
			action: 'getComentariosVideo',
			accessToken: App.accessToken,
			idVideo: id,
			userID: App.userID
		});
		
		posting.done(function(data)
		{
			consoleLog(data);
			
			$('#comentarios').html(data);
			$('#comentarios').show();
			
			/*$('#comentarios').imagesLoaded()
			.always( function( instance )
			{*/
				
				
				$('.textfield').focus(function()
				{
					if ($(this).val()=='Escribe un comentario...')
					{
						$(this).val('')
					}
				});
				
				$('.textfield').blur(function()
				{
					if ($(this).val()=='')
					{
						$(this).val('Escribe un comentario...')
					}
				});
				
				$('.like').each(function ()
				{
					var time = jQuery.timeago($(this).data('time'));
					$(this).html(time);
				});
				
				App._attachActionEvents();
			//});
		});
	}
	
	this.isMobile = function()
	{
	    var mobile = (/iphone|ipad|ipod|android|blackberry|mini|windows\sce|palm/i.test(navigator.userAgent.toLowerCase()));
    
	    return mobile;
	}
	
	this.startPlayer = function(id)
	{
		consoleLog('App.videoHash '+App.videoHash);
		
		if (App.videoHash != '')
		{
			
			if ($('.youtubeVideo:first').data('param') == App.videoHash)
			{
				App.getComentarios(id);
				$('#player').fadeIn();
				
				if (App.isMobile())
				{
					$('#player').html('<iframe id="youtube" width="680" height="418" src="https://www.youtube.com/embed/'+id+'?enablejsapi=1&wmode=transparent&showinfo=0&autohide=1" frameborder="0" allowfullscreen></iframe>');
				}
				else
				{
					$('#player').html('<object width="680" height="418"><param name="movie" value="https://www.youtube.com/v/'+id+'?version=3&amp;hl=es_MX"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="https://www.youtube.com/v/'+id+'?version=3&amp;hl=es_MX" type="application/x-shockwave-flash" width="680" height="418" allowscriptaccess="always" allowfullscreen="true"></embed></object>');
				}
				
				//App.onYouTubeIframeAPIReady();				
				App.videoHash = '';
			}
			else
			{
				App.carruselNext();
			}
				
			
		}
		else
		{
			App.getComentarios(id);
			$('#player').fadeIn();
			
			if (App.isMobile())
			{
				$('#player').html('<iframe id="youtube" width="680" height="418" src="https://www.youtube.com/embed/'+id+'?enablejsapi=1&wmode=transparent&showinfo=0&autohide=1" frameborder="0" allowfullscreen></iframe>');
			}
			else
			{
				$('#player').html('<object width="680" height="418"><param name="movie" value="https://www.youtube.com/v/'+id+'?version=3&amp;hl=es_MX"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="https://www.youtube.com/v/'+id+'?version=3&amp;hl=es_MX" type="application/x-shockwave-flash" width="680" height="418" allowscriptaccess="always" allowfullscreen="true"></embed></object>');
			}
			//$('#player').html('<iframe id="youtube" width="680" height="418" src="https://www.youtube.com/embed/'+id+'?enablejsapi=1&wmode=transparent&showinfo=0&autohide=1" frameborder="0" allowfullscreen></iframe>');
			//App.onYouTubeIframeAPIReady();
		}
		
		
		
		
	}
	
	this.showVideo = function(event)
	{
		var el 	= $(event.delegateTarget);
		var id  = el.data('param');	
		
		App.startPlayer(id);
	}
	
	/*
	 * Inserta el comentario de la página a la BD
	 */
	this.insertarComentario = function(event)
	{
		var val = $('.textfield').val();
		if (val=='' || val=='Escribe un comentario...')
		{
			alert('Favor de escribir un comentario.');
		}
		else
		{
			var id = $(event.delegateTarget).data("pagina");
			
			var posting = $.post(App.intfc,
			{
				action: 'insertComentarios',
				accessToken: this.accessToken,
				pagina: id,
				comentario: $('.textfield').val(),
				userID:	App.userID
			});
			
			posting.done(function(data)
			{
				consoleLog(data);
				App.getComentarios(id);
			});	
		}
		
	}
	
	/*
	 * Valida el voto del comentario
	 */
	this.votar = function(event)
	{
		var pagina 	= $(event.delegateTarget).data("pagina");
		
		var posting = $.post(App.intfc,
		{
			action: 'insertVoto',
			accessToken: this.accessToken,
			pagina: pagina
		});
		
		posting.done(function(data)
		{
			consoleLog(data);
			
			if (data!='false')
			{
				App.getComentarios(pagina);
			}
			else
			{
				$(event.delegateTarget).append('Ya votaste por este comentario');
			}
			
			
		});
	}
	
	this.onYouTubeIframeAPIReady =function()
	{
		App.youtubePlayer = new YT.Player('youtube',
		{
			events:
			{
				'onReady': App.onPlayerReady,
				'onStateChange': App.onPlayerStateChange
			}
		});
	}
	
	// The API will call this function when the video player is ready.
	this.onPlayerReady = function(event)
	{
		consoleLog('player ready, esperar 1s y play ');
		setTimeout(function()
		{
			if($('#youtube').length>0)
				event.target.playVideo(); 
		},1000);
	}
	
	// The API calls this function when the player's state changes.
	// The function indicates that when playing a video (state=1),
	// the player should play for six seconds and then stop.
	this.onPlayerStateChange = function(event)
	{            
		
	}
		
	this.stopVideo = function()
	{            
	    App.youtubePlayer.stopVideo();
	}
	
	/*
	 * Inserta el video con un delay del play
	 */
	this.youtubeVideo = function()
	{
		App.FB.Canvas.scrollTo(0,0);
		
		$('#dynamic').animate(
		{
			left: 1000
		},1000);
		var posting = $.post(App.intfc,
		{
			action: 'getYoutubeVideo',
			accessToken: this.accessToken
		});
		
		posting.done(function(data)
		{
			$('#dynamic').html(data);
			
			$('.dynamic').imagesLoaded()
			.always( function( instance )
			{
				$('#dynamic').animate(
				{
					left: 0
				},1000);
				App.onYouTubeIframeAPIReady();
			});
			
			App._attachActionEvents();			
		});
	}
	
	this.showAviso = function()
	{
		App.FB.Canvas.scrollTo(0,0);
		$('.terminosTit').html('Aviso Legal');
		$('.terminos').fadeIn();
		$('.terTxt').hide();
		$('.aviTxt').fadeIn();
	}
	
	this.showTerminos = function()
	{
		App.FB.Canvas.scrollTo(0,0);
		$('.terminosTit').html('Términos y Condiciones');
		$('.terminos').fadeIn();
		$('.aviTxt').hide();
		$('.terTxt').fadeIn();
	}
	
	this.showShare = function()
	{
		//var win = window.open('https://www.facebook.com/iceshotmx', '_blank');
		//win.focus();
		App.postFB();
	}
	
	this.closeTerminos = function()
	{
		$('.terminos').fadeOut();
	}
	
	/*
	 * Función para generar el post en FB
	 */
	this.postFB = function ()
	{
		
		var picture = 'http://appnet-social.com/iceshot/shots/assets/img/share.jpg';
		//var picture = 'http://bonobo-projects.com/doubleyou/iceshot/assets/images/share.jpg';
		
		
		consoleLog('video id: '+App.videos[App.videosIndice]);
		
		//doubleyoudemo/app_1440064216248578
		var url ='https://www.facebook.com/iceshotmx/app_1440064216248578?app_data='+App.videos[App.videosIndice]
		//var url ='https://www.facebook.com/BonoboDigitalDemo/app_273364706182238?app_data='+App.videos[App.videosIndice]
		
		var posting = $.post(App.intfc,
		{
			action: 'getShortUrl',
			accessToken: this.accessToken,
			url:	url
			
		});
		
		posting.done(function(data)
		{
			var obj =
			{
				method: 	'feed',
				name: 		'Los shots de Ice Shot',
				link: 		data,		
				picture: 	picture,
				caption: 	'Ice Shot',
				description: 	'Ellos pensaban vivir un día normal hasta que se enfrentaron a los Shots de Ice Shot. A veces, sólo un Ice Shot puede salvarte.',
				display:	'popup'
			};
			
			App.FB.ui(obj,
			function(response)
			{
				consoleLog('post response');
			});
		});
	}
	
	
	// ------------------------------------------------------------------------------------------
	// ------------------------------------------------------------------------------------------
	// No Modificar los métodos siguientes, son comúnes para todas las apps.
	// ------------------------------------------------------------------------------------------
	// ------------------------------------------------------------------------------------------
		
	/**
	 * Registra los eventos onClick en todos aquellos nodos que tengan definido el atributo 'data-action'
	 *
	 * Además de registrar los eventos toma el valor del atributo para generar la función que se ejecutará en el evento.
	 */
	this._attachActionEvents = function() 
	{
		if(DEBUG) console.log('...App.attachActionEvents();');
		$('*[data-action]').unbind( "click" );
	
		$('*[data-action]').click(function(event)
		{
			var data 	= $(this).attr( "data-action" );			
			window['App'][data](event);
		});
	}; // EOF
	
	/**
	 * Devuelve el valor del parámetro sParam en el url
	 */
	this._GetURLParameter = function(sParam)
	{
		if(DEBUG) console.log('...App.GetURLParameter("' + sParam + '")');
		
		var sPageURL 		= window.location.search.substring(1);
		var sURLVariables 	= sPageURL.split('&');
		
		var resp = '';

		for (var i = 0; i < sURLVariables.length; i++)
		{	
			var sParameterName = sURLVariables[i].split('=');

			if(sParameterName[0] == sParam)
			{
				resp = sParameterName[1];
			}
		}
		
		return resp;
	}; // EOF
	
	this.setHash = function(hash)
	{
		location.hash = hash;
	}
	
	this.getHash = function()
	{
		return location.hash.slice(1);
	}
};

