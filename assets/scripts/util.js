DelayedTask = function(fn, scope, args)
{
	var me = this,
        id,     
        call = function()
	{
		clearInterval(id);
		id = null;
		fn.apply(scope, args || []);
        };            
    
	me.delay = function(delay, newFn, newScope, newArgs)
	{
		me.cancel();
		fn = newFn || fn;
		scope = newScope || scope;
		args = newArgs || args;
		id = setInterval(call, delay);
	};

    
	me.cancel = function()
	{
		if(id)
		{
		    clearInterval(id);
		    id = null;
		}
	};
};

/*
 var task = new DelayedTask(function()
 {
   
 });			  
 task.delay(200);
 */

function shuffle(array)
{
    var currentIndex = array.length
    , temporaryValue
    , randomIndex
    ;
    
    // While there remain elements to shuffle...
    while (0 !== currentIndex) {
    
    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;
    
    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
    }
    
    return array;
}

function getRandom(min, max)
{
    return Math.random() * (max - min) + min;
}

function showElement(div)
{
	
	
	
   var el = $(div); 
   if ( el.is(":visible") )
   {
      el.fadeOut();
   }
   else
   { 
      el.fadeIn();
   }
}