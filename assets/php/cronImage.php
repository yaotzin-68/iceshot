<?php 
error_reporting(E_ALL);
ini_set('display_errors', '1');
require_once("blackbox/BLACKBOX.php"); ?>
<?php 
	echo 'cron...<hr>';
	
	$conn 		= new DB_Connection();	
	$db		= $conn->connect();
		
	$sql	= "SELECT *
			 FROM tiarosa_diadelatia_frases, tiarosa_diadelatia_tokens 
			 WHERE tiarosa_diadelatia_tokens_facebook_id = tiarosa_diadelatia_frases_id_facebook
			 ORDER BY tiarosa_diadelatia_frases_id DESC
		    ";
	
	$rs 	= $db->Execute($sql) or die('Error en sql: ' . $db->ErrorMsg());
	
	while(!$rs->EOF)
	{
		
		$data['fraseGanadora'] 		= $rs->Fields('tiarosa_diadelatia_frases_frase_ganadora');
		$data['facebookIdUsuario'] 	= $rs->Fields('tiarosa_diadelatia_frases_id_facebook');
		$data['facebookIdTia'] 		= $rs->Fields('tiarosa_diadelatia_frases_id_tia');
		$data['nombreUsuario'] 		= $rs->Fields('tiarosa_diadelatia_frases_nombre');
		$data['nombretia'] 		= $rs->Fields('tiarosa_diadelatia_frases_nombre_tia');
		$data['frase'] 			= $rs->Fields('tiarosa_diadelatia_frases_frase');		
		$data['mensaje'] 		= 'Mensaje de la App'; 										// Establecer mensaje predefinido
		$data['accessToken'] 		= $rs->Fields('tiarosa_diadelatia_tokens_access_token');
		$data['frase_id'] 		= $rs->Fields('tiarosa_diadelatia_frases_id');
		
		
		$data['foto']			= 'assets/apps/diadelatia/'.$rs->Fields('tiarosa_diadelatia_frases_imagen_post'); 		// Cambiar por la foto real	
		
		
		if (!file_exists($data['foto']))
		{
			echo '<pre>' . print_r($data, 1) . '</pre>';
		   
			echo '<br>no existe archivo'; 
			if($data['fraseGanadora']==0)
			{
				$destino 		= imagecreatefromjpeg('assets/apps/diadelatia/assets/images/generadas/'.$data['frase'].'f.jpg'); 
			}
			else
			{
				$sql 		= "SELECT *
							FROM tiarosa_diadelatia_frases,core_cloudfiles,tiarosa_diadelatia_comentarios
							WHERE tiarosa_diadelatia_frases_frase_ganadora = 1
							AND tiarosa_diadelatia_frases_frase = tiarosa_diadelatia_comentarios_id
							AND core_cloudfiles_id = tiarosa_diadelatia_comentarios_id_imagen
							AND tiarosa_diadelatia_frases_id = ".$data['frase_id'] ;
				//echo $sql.'<br>';
				$rs1 		= $db->Execute($sql) or die('Error en sql: ' . $db->ErrorMsg());
				
				$archivoCache 	= md5('core_cloudfiles_'.$rs1->Fields('tiarosa_diadelatia_comentarios_id_imagen'));			
				$destino 	= imagecreatefromjpeg("https://porsuricosaborcasero.com/assets/cache/".$archivoCache.".".$rs1->Fields('core_cloudfiles_extension')); 
			}
			
			$nom 			= explode(' ',substr($data['nombretia'],0,13));
	
			$nombreGenerada         = md5('generada_'.$data['frase_id']);
						
			$text 			= 'A MI TÍA '.strtoupper($nom[0]).':'; 	// El nombre del amigo taggeado		
			$font 			= 'assets/apps/diadelatia/assets/fonts/GeorgiaBoldItalic';			// Donde va la fuente, esta metela dentro de assets/fonts
			$fontSize		= 20;					// Tamaño de la fuente en puntos
			$fontWidth 		= imagefontwidth($fontSize);		// El ancho de la fuente en pixeles
			$textWidth		= ($fontWidth * strlen($text))*1.7;	// El tamaño del texto en pixeles
			$positionCenter 	= ceil((800 - $textWidth) / 2);		// Calcula la posición en la que se dibujará el texto horizontalmente  en relación al tamaño del contenedor de la imagen		
			$blanco 		= imagecolorallocate($destino, 116, 14, 9);	// Color blanco
			
			imagettftext($destino, $fontSize, 0, $positionCenter, 87, $blanco, $font, $text); 	// Va el texto con la sombra (font: 15, x: 20, y: 37)
			
			imagealphablending($destino, true);
			imagesavealpha($destino, true);
				
			imagejpeg($destino, 'assets/apps/diadelatia/assets/cache/'.$nombreGenerada.'.jpg'); 			
			imagedestroy($destino);
			
			$sql = "UPDATE tiarosa_diadelatia_frases
				SET
					tiarosa_diadelatia_frases_imagen_post	= 'assets/cache/".$nombreGenerada.".jpg'
				WHERE   tiarosa_diadelatia_frases_id		= ".$data['frase_id'];
				
			$db->Execute($sql);			
			echo 'imagen creada:'.'assets/apps/diadelatia/assets/cache/'.$nombreGenerada.'.jpg';
		}
		
		
		$rs->MoveNext(); 
		
	}		
				
?>