<?php
/**
 * Archivo de la clase Sql
 * 
 * LICENCIA:
 * 
 * Este programa es software libre: usted puede redistribuirlo y/o modificarlo 
 * bajo los términos de la Licencia Pública General GNU publicada 
 * por la Fundación para el Software Libre, ya sea la versión 3 
 * de la Licencia, o (a su elección) cualquier versión posterior.
 *
 * Este programa se distribuye con la esperanza de que sea útil, pero 
 * SIN GARANTÍA ALGUNA; ni siquiera la garantía implícita 
 * MERCANTIL o de APTITUD PARA UN PROPÓSITO DETERMINADO. 
 * Consulte los detalles de la Licencia Pública General GNU para obtener 
 * una información más detallada. 
 *
 * Debería haber recibido una copia de la Licencia Pública General GNU 
 * junto a este programa. 
 * En caso contrario, consulte <http://www.gnu.org/licenses/>.
 * 
 * @package 	conf
 * @copyright 	2011 chukan
 * @version		conf.php, v 1.0 10-may-2011 chukan.net
 * @author		Alberto Fuentes <alberto@chukan.net>
 * @filesource
 * @license		GNU/GPL
 */

/**
 * Sql
 * 
 * Clase usada para la extracción y despliegue de articulos de un RSS
 * 
 * @package 	SDK
 * @subpackage	Util
 */
class Sql 
{	
	/**
	 * @var string tipo de la RDBMS
	 */
	var $_DBTYPE;

	/**
	 * @var string servidor de la RDBMS
	 */
	var $_DBHOST;

	/**
	 * @var string nombre de la instancia de la RDBMS
	 */
	var $_DBNAME;

	/**
	 * @var string usuario de la RDBMS
	 */
	var $_DBUSER;

	/**
	 * @var string password del usuario
	 */
	var $_DBPASS;
	
	var	$conn;
	/**
	 * Constructor
	 */
	function __construct( $dbtype = null, $dbhost = NULL, $dbname = NULL, $dbuser = NULL, $dbpass = NULL )
	{
		$this->_DBTYPE = (is_null($dbtype)) ? DBTYPE : $dbtype;
		$this->_DBHOST = (is_null($dbhost)) ? DBHOST : $dbhost;
		$this->_DBNAME = (is_null($dbname)) ? DBNAME : $dbname;
		$this->_DBUSER = (is_null($dbuser)) ? DBUSER : $dbuser;
		$this->_DBPASS = (is_null($dbpass)) ? DBPASS : $dbpass;
	} # END of function
	
	function connect()
	{
		$this->conn = new mysqli($this->_DBHOST, $this->_DBUSER, $this->_DBPASS,$this->_DBNAME);
		$this->conn->set_charset("utf8");
		
		if ($this->conn->connect_errno)
		{
		    echo "Fallo al contenctar a MySQL: (" . $this->conn->connect_errno . ") " . $this->conn->connect_error;
		}
		
		return $this->conn;
	}
	
	function getID()
	{
		return $this->conn->insert_id;
	}
	
	function realEscapeString($data)
	{
		return $this->conn->real_escape_string($data);
	}
	
	function execute($query)
	{	
		$result = $this->conn->query($query);
		return $result;
	}
	
	function disconnect()
	{
		$this->conn->close();
	}
	
	function getDBHOST() {
		return $this->_DBHOST;
	} # END of function
	
	function getDBNAME() {
		return $this->_DBNAME;
	} # END of function
	
} # END of class
?>