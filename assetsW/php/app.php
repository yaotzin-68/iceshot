<?php

//error_reporting(E_ALL);
//ini_set('display_errors', '1');

/**
 * Inclusión del sdk para php de facebook
 */
require_once('../libraries/facebook-php-sdk/src/facebook.php');

//Valores para conección de la BD y la clase para conectarse.
require_once('conf.php');

/**
 * Definición de la app
 */
class App
{
	/**
	 * facebook
	 * 
	 * @var obj
	 * @access public
	 */
	public $facebook;
	
	/**
	 * curl
	 * 
	 * @var mixed
	 * @access private
	 */
	private $curl;
	
	/**
	 * app_id
	 * 
	 * @var mixed
	 * @access public
	 */
	public $app_id;
	
	/**
	 * app_secret
	 * 
	 * @var mixed
	 * @access public
	 */
	public $app_secret;
	
	/**
	 * app_namespace
	 * 
	 * @var mixed
	 * @access public
	 */
	public $app_namespace;
	
	/**
	 * scope
	 * 
	 * @var mixed
	 * @access public
	 */
	public $scope;
	
	/**
	 * $amigosFB
	 * 
	 * @var mixed
	 * @access public
	 */
	public $amigosFB;
	
	/**
	 * $amigosJSON
	 * 
	 * @var mixed
	 * @access public
	 */
	public $amigosJSON;
	
	/**
	 * Constructor
	 */
	function __construct()
	{
            
            $this->app_id 			= FBAPPID;
            $this->app_secret 		        = FBAPPSECRET;
            $this->scope 			= FBSCOPE;
            $this->curl 			= curl_init();
            $this->amigosFB			= '';
            $this->amigosJSON		        = '';
	 
	    $this->facebook = new Facebook(
                    array
                    (
                        'appId'  => $this->app_id,
                        'secret' => $this->app_secret,
                    )
            );
		
	} # eof
	
       public function getFBID()
       {
            echo FBAPPID.'#'.FBSCOPE;
       }
       
       public function doLoginFB()
       {
	    $user_id = $this->facebook->getUser();
	    
	    if($user_id)
	    {
		    /*
		    $signed_request = $facebook->getSignedRequest();
		    
		    $app_data = '';
		    if(isset($signed_request["app_data"]))
		    {
			    $app_data = $signed_request["app_data"];
			    header('Location: index.html?app_data='.$app_data);
		    }
		    else
		    {
			    header('Location: index.html');	
		    }*/
		    
		    echo $this->getAccessToken();		    
	    }
	    else
	    {
		$login_url = $this->facebook->getLoginUrl
		(
			array
			( 
				'scope' 	=> FBSCOPE,
				'redirect_uri'  => FBSITEURL
			)
		);
		
		echo $login_url;
	    }
	    
       }
       
        public function getVideos()
	{
	    $json 	= file_get_contents('http://appnet-social.com/iceshot/shots/assets/json/videos.json');	    
	    $data  	= (array) json_decode($json);
	    
	    $resp = array();	    
	    for($i=0;$i<count($data);$i++)
	    {
		array_push($resp,$data[$i]->videoID);
	    }
	    
	    echo implode(',',$resp);
	}
        
	
	public function getHome($accessToken)
	{
            $tpl = $this->getTemplate('tpl_home.html');
            echo $tpl;
	}
        
        public function getComentariosVideo($idVideo,$user_id)
	{
	    //$user_id 		= $this->facebook->getUser();
	    $user 		= '';
	    $foto 		= '';
            $accessToken 	= $this->getAccessToken();
            $votos 		= 0;       
            $comentarios 	= '';
	    
            $sqlConect = new Sql();
            $sqlConect->connect();
            
            $sql = "SELECT *
                            FROM iceshot_comentarios,iceshot_userfb
                            WHERE iceshot_comentarios_id_pagina = '".$idVideo."'
			    AND iceshot_userfb_id_fb = iceshot_comentarios_id_facebook
                            ORDER BY iceshot_comentarios_fecha_creacion DESC";
            $result = $sqlConect->execute($sql);
           
	    if($user_id)
	    {
		//echo '$user_id '.$user_id;
		
		//$user   = $this->getUserData($accessToken);
		$foto   = '<img src="https://graph.facebook.com/'.$user_id.'/picture">';        
		
	    }
            
            while ($row = $result->fetch_assoc())
            {
                    $timestamp 	= $row['iceshot_comentarios_fecha_creacion'];
                    
                    $dataID['name'] = $row['iceshot_userfb_name'];
                    
                    $comentarios.=	'
                            <div class="row" style="width:inherit; margin:0;">
				<div class="small-2 large-2 columns no-margin fotoUserComent">
				    <img src="https://graph.facebook.com/'.$row['iceshot_comentarios_id_facebook'].'/picture">
				</div>
                                <div class="small-10 large-10 columns no-margin infoUserComent">
				    <span class="nombre">'.$dataID['name'].'</span>
				    <span class="coment">'.$row['iceshot_comentarios_comentario'].'</span>
                                    <div class="like" data-time="'.$timestamp.'"></div>    
				</div>
                            </div>
			    <hr>
                    ';
                    
            }
            
            $data['#foto#']             = $foto;
            $data['#comentarios#'] 	= $comentarios;
            $data['#pagina#'] 	        = $idVideo;
		
            $tpl = $this->getTemplate('tpl_comentarios.html');
            $tpl = str_replace(array_keys($data),array_values($data),$tpl);
            	
            echo $tpl;
	}
        
        public function getShortUrl($url)
        {
            $shortUrl  = $this->get_bitly_short_url($url);
            
            echo $shortUrl;
        }
        
        /*
	 * Devuelve el template de la sección del video de Youtube
	 */
	public function getYoutubeVideo($accessToken)
	{
            $tpl = $this->getTemplate('tpl_youtube.html');		
            echo $tpl;
	}
        
        /*
	 * Devuelve el template de la sección del video de Youtube
	 */
	public function facebookComments($accessToken)
	{
            $tpl = $this->getTemplate('tpl_comments.html');		
            echo $tpl;
	}
	
	/*
	 * Devuelve el código de la sección de la Galería
	 */
	public function getGaleria($accessToken,$pagina)
	{
		$sqlConect 	= new Sql();
		$sqlConect->connect();
		
		$sql 		= "SELECT *
					FROM iceshot_comentarios,core_cloudfiles
					WHERE iceshot_comentarios_frase_publicada = 1
					AND core_cloudfiles_id = iceshot_comentarios_id_imagen
					ORDER BY iceshot_comentarios_fecha_modificacion DESC 
				   ";
		//echo $sql;
		$result 	= $sqlConect->execute($sql);
		
		$galeria  = array();
		$total    = 0;
		$carrusel = '';
		
		if($result !== FALSE)
		{
			while ($row = $result->fetch_assoc())
			{
				$archivoCache 	= md5('core_cloudfiles_'.$row['iceshot_comentarios_id_imagen']);			
				
				$src 		= "https://graph.facebook.com/" . $row['iceshot_comentarios_from_id']."/picture";
				$thumb = '
				    <div class="galeriaThumb" data-id="'.$row['iceshot_comentarios_id'].'" data-action="gotoFraseGanadora">
					<div class="thumbFoto"><img src="https://porsuricosaborcasero.com/assets/cache/'.$archivoCache.'_thumb.'.$row['core_cloudfiles_extension'].'"></div>
					<div class="thumbFotoUsuario"><img src="'.$src.'"></div>
					<span>'.$row['iceshot_comentarios_from_name'].'</span>
				    </div>
				';
				$total++;
				
				array_push($galeria,$thumb);
			}
		}
		
		for($i=1;$i<21;$i++)
		{
			
			$thumb = '
			    <div class="galeriaThumb" data-id="'.$i.'" data-action="gotoMotivos">
				<div class="thumbFoto"><img src="assets/images/panel2/'.$i.'.jpg"></div>
				<div class="thumbFotoUsuario"><img src="assets/images/panel3/tiaRosa.png"></div>
				<span>Tía Rosa</span>
			    </div> 
			';
			array_push($galeria,$thumb);
		}
		
		shuffle($galeria);
		
		$arrayTmp 	= array_chunk($galeria, 9);
		$paginas 	= ceil(count($galeria)/9);
		$contGalerias	= '';
		for($i=0;$i<$paginas;$i++)
		{
			$contGalerias .= '<div class="galeriaPagina">'.implode("", $arrayTmp[$i]).'</div>';			
		}
		
		$paginacion = '';		
		for($i=0; $i < $paginas; $i++)
		{
			if(($i+1) == $pagina)
				$selected = 'selected';
			else
				$selected = '';
				
			$paginacion .= '<div class="galeriaPagBtn '.$selected.'" data-action="galeriaPaginacion" data-pagina="'.($i+1).'">'.($i+1).'</div>';
		}
		
		$data['#galeria#'] 	= $contGalerias;
		$data['#paginacion#'] 	= $paginacion;
		
		$tpl = $this->getTemplate('tpl_galeria.html');
		$tpl = str_replace(array_keys($data),array_values($data),$tpl);
		echo $tpl;
	}
	
	/*
	 * Paginación para la lista de amigos
	 */
	public function cargarAmigos($accessToken,$pagina)
	{
		//echo 'cargar amigos '.$pagina;
		
		$amigos1 = $this->getFriendsPics($accessToken,21,($pagina*21));
		
		$amigosHtml = '';
		foreach ($amigos1 as $friend)
		{
			$amigosHtml .= '
			    <div id="'.$friend[2].'" class="amigo">
					<img src="'.$friend[0].'" >
					<div class="amigoTabla">
						<table >
							<tr>
								<td valign="middle">
									'.$friend[1].'
								</td>
							</tr>
						</table>
					</div>
				    <input type="hidden" class="hidden" value="'.$friend[2].'">
			    </div>
			';
		}
		
		echo $amigosHtml;
	}
	
	
        
        /*
         * Genera 1 imagen dinámica, pone nombre y junta 1 png sobre un fondo en jpf
         */
        public function generarImagen($accessToken,$imagen,$background,$amigoId)
        {
            $data           = $this->getUserData($accessToken);
            $dataAmigo      = $this->getDataId($amigoId);
            
            $destino 	    = imagecreatefromjpeg($background); 
            $fuente 	    = imagecreatefrompng($imagen); 	    
            
            $fuenteTam 	    = getimagesize($imagen);
            $posX	    =0; 400 - ($fuenteTam[0]/2);
            $posY	    =0; (400 - ($fuenteTam[1]/2))+ 20;
                                    
            $text 	    = 'Para: '.$dataAmigo['name'];              // El nombre del amigo taggeado
            $font 	    = 'assets/fonts/GeorgiaBoldItalic'; 	// Donde va la fuente, esta metela dentro de assets/fonts
            $fontSize	    = 20;					// Tamaño de la fuente en puntos
            $fontWidth 	    = imagefontwidth($fontSize);		// El ancho de la fuente en pixeles
            $textWidth	    = ($fontWidth * strlen($text)) * 1.66;	// El tamaño del texto en pixeles
            $positionCenter = ceil((770 - $textWidth) / 2);		// Calcula la posición en la que se dibujará el texto horizontalmente
                                                                        // en relación al tamaño del contenedor de la imagen
            
            $blanco 	= imagecolorallocate($destino, 255, 255, 255);	// Color blanco
            $negro	= imagecolorallocate($destino, 0, 0, 0);	// Color negro
            
            imagettftext($destino, $fontSize, 0, $positionCenter, 50, $negro, $font, $text); 	// Va el texto con la sombra (font: 15, x: 20, y: 37)
            imagettftext($destino, $fontSize, 0, ($positionCenter - 1), 50, $blanco, $font, $text); // Va el texto blanco (font: 15, x: 20, y: 37)            
            
            imagealphablending($destino, true);
            imagesavealpha($destino, true);
            
            imagealphablending($fuente, true);
            imagesavealpha($fuente, true);
            
            $this->imagecopymerge_alpha($destino, $fuente, $posX, $posY, 0, 0, $fuenteTam[0], $fuenteTam[1], 100);
            
            header('Content-Type: image/png'); // Este header solo es para mostrar la imagen en pantalla, no lo necesitas sólo la funcion de abajo...
            
            # El segundo parametro es donde se guardará la imagen junto con el nombre, este nombre de imagen generalo con el id de facebook
            # del usuario y puede ser el nombre de la galleta combinado con la imegan de fondo y un autonumérico, por aquello de que pueda generar
            # 'n' cantidad de galletas...
            $archivoFinal = $this->nombreImagenGenerada('assets/cache/'.$amigoId.'.png');
            
            imagepng($destino, $archivoFinal);
            imagedestroy($destino);
            imagedestroy($fuente);
            
            return $archivoFinal;
        }
        
        /*
         * Genera foto dinámica, la sube a FB y etiqueta al amigo seleccionado.
         */
        public function etiquetarAmigo($accessToken,$imagen,$background,$amigoId)
        {
            $img = $this->generarImagen($accessToken,$imagen,$background,$amigoId);
            
            $this->postPhoto('mensaje',$img,$amigoId);
        }
        
        
        
        /*
         * Función para validar que no existe el nombre de la imagen y regresar 1 nombre que no exista
         */
        public function nombreImagenGenerada($archivo)
        {
            $archivoValido = false;
            
            while(!$archivoValido)
            {
                if (file_exists($archivo))
                {
                    $pos = strrpos($archivo, "-");
                    
                    if ($pos === false)
                    {
                        $archivo = explode('.png',$archivo);
                        $archivo = $archivo[0].'-1.png';                    
                    }
                    else
                    {
                        //assets/cache/123456-5.png
                        $tmp = explode('-',$archivo);
                        $tmp1 = explode('.png',$tmp[1]);
                        $ind = intval($tmp1[0]);
                        $ind++;
                        
                        $archivo = $tmp[0].'-'.$ind.'.png';
                    }
                }
                else
                {
                    $archivoValido = true;
                }           
            }
            
            return $archivo;
            
           
            
        }
        /**
        * Copia una imagen con canal alpha dentro de otra imagen, reemplazo de la función php imagecopymerge
        */
        public function imagecopymerge_alpha($dst_im, $src_im, $dst_x, $dst_y, $src_x, $src_y, $src_w, $src_h, $pct)
        { 
            $cut = imagecreatetruecolor($src_w, $src_h);             
            imagecopy($cut, $dst_im, 0, 0, $dst_x, $dst_y, $src_w, $src_h);             
            imagecopy($cut, $src_im, 0, 0, $src_x, $src_y, $src_w, $src_h);             
            imagecopymerge($dst_im, $cut, $dst_x, $dst_y, 0, 0, $src_w, $src_h, $pct); 
        } 
        
        /**
	 * Regresa la lista de amigos.
	 */ 
	public function loadListAmigos($accessToken)
	{
            if($this->amigosFB == '')
            {
                    $amigos  = $this->getFriendsList($accessToken);
                    $amigos1 = $this->getFriendsPics($accessToken,21,0);
            
                    $amigosJsn = '[';
                    $amigosHtml = '';
                    
                    foreach ($amigos as $friend)
                    {
                            $amigosJsn.='{"id":'.$friend[2].',"name":"'.$friend[1].'","picture":"'.$friend[0].'"},';
                    }
                    
                    foreach ($amigos1 as $friend)
                    {
                            $amigosHtml .= '
                                <div id="'.$friend[2].'" class="amigo">
                                            <img src="'.$friend[0].'" >
                                            <div class="amigoTabla">
                                                    <table width="100%" height="69">
                                                            <tr>
                                                                    <td valign="middle">
                                                                            '.$friend[1].'
                                                                    </td>
                                                            </tr>
                                                    </table>
                                            </div>
                                        <input type="hidden" class="hidden" value="'.$friend[2].'">
                                </div>
                            ';
                    }
                    
                    $amigosJsn .= ']';
                    
                    $this->amigosFB		= $amigosHtml;
                    $this->amigosJSON		= $amigosJsn;
            }
            
            $data['#amigosContent#'] 	= $this->amigosFB;
            $data['#amigosJSON#']	= $this->amigosJSON;
        
            $tpl = $this->getTemplate('tpl_amigos.html');
            $tpl = str_replace(array_keys($data),array_values($data),$tpl);
            echo $tpl;
	    
	} # eof
    
	
        /*
         * Regresa los comentarios de la BD
         */
        public function comentarios($accessToken,$pagina)
        {
            $sqlConect = new Sql();
            $sqlConect->connect();
            
            $sql = "SELECT *
                            FROM iceshot_comentarios
                            WHERE iceshot_comentarios_id_pagina = '".$pagina."'
                            ORDER BY iceshot_comentarios_fecha_creacion DESC";
            $result = $sqlConect->execute($sql);
            //echo $sql;
                            
            $comentarios = '';
            while ($row = $result->fetch_assoc())
            {
                    $timestamp 	= $row['iceshot_comentarios_fecha_creacion'];
                    $tiempo 	= $this->time_delta($timestamp);
                    
                    $dataID     = $this->getDataId($row['iceshot_comentarios_id_facebook']);
                    
                    $sql = "SELECT count(*) as votos
                            FROM iceshot_votos
                            WHERE iceshot_votos_id_pagina ='".$pagina."'";
                    //echo $sql.'<br>';
                    $rs     = $sqlConect->execute($sql);							
                    $rw     = $rs->fetch_assoc();
                    $votos  = $rw['votos'];	
                    
                    $comentarios.=	'
                            <div class="comentario">
                                    <img src="https://graph.facebook.com/'.$row['iceshot_comentarios_id_facebook'].'/picture">
                                    <div class="comentarioBd">
                                        <span class="nombre">'.$dataID['name'].'</span>
                                        <span class="coment">'.$row['iceshot_comentarios_comentario'].'</span>
                                        <div class="like" data-time="'.$timestamp.'">                                            
                                            '.$tiempo.'
                                        </div>    
                                    </div>
                            </div>
                            <hr>
                    ';
            }
            
            $data['#comentarios#'] 	= $comentarios;
            $data['#pagina#'] 	        = $pagina;
            $data['#votos#'] 	        = $votos;
		
            $tpl = $this->getTemplate('tpl_comentarios.html');
            $tpl = str_replace(array_keys($data),array_values($data),$tpl);
            echo $tpl;
        }
        
        
        /*
         * Inserta el comentario en la BD
         */
        public function insertComentarios($accessToken,$pagina,$comentario,$facebookId)
        {
            $sqlConect = new Sql();
            $sqlConect->connect();
            
            //$facebookId		= $this->getUserID($accessToken);
            
            $sql = "INSERT iceshot_comentarios
                    SET
                            iceshot_comentarios_id_facebook		= ".$facebookId.",						
                            iceshot_comentarios_id_pagina       	= '".$pagina."',
                            iceshot_comentarios_comentario		= '".$comentario."',
                            iceshot_comentarios_autor 			= 'insertComentario',
                            iceshot_comentarios_editor 			= 'insertComentario',
                            iceshot_comentarios_autor_sessid 		= '".$accessToken."',
                            iceshot_comentarios_editor_sessid	 	= '".$accessToken."',
                            iceshot_comentarios_fecha_creacion 		= '".$this->getTime()."',
                            iceshot_comentarios_fecha_modificacion	= '".$this->getTime()."'";
            
	    $banned_words = array('insert','delete','update','select','script');
	    	
	    
	    if ($this->teststringforbadwords(strtolower($comentario),$banned_words))
	    {
		mysqli_real_escape_string($sql);
		$sqlConect->execute($sql);
	    }
	    
	    echo $sql;
	    
            //$this->comentarios($accessToken,$pagina);
        }
	
	public function teststringforbadwords($string,$banned_words)
	{
	    foreach($banned_words as $banned_word)
	    {
		
		if(stristr($string,$banned_word))
		{
		    return false;
		}
	    }
	    
	    return true;
	}

        /*
         * Inserta el comentario en la BD
         */
        public function insertVoto($accessToken,$pagina)
        {
            $sqlConect = new Sql();
            $sqlConect->connect();
            
            $data   = $this->getUserData($accessToken);
            
            $sql = "SELECT count(*) as votos
                           FROM iceshot_votos
                           WHERE iceshot_votos_id_pagina = '".$pagina . "'
                           AND iceshot_votos_id_facebook =".$data['id'] ;
             
            $rs     = $sqlConect->execute($sql);							
            $rw     = $rs->fetch_assoc();
            $votos  = $rw['votos'];
            
            //echo 'votos: '.$votos;
            
            if($votos==0 || $votos=='0')
            {
                $sql = "INSERT iceshot_votos
                    SET
                            iceshot_votos_id_facebook		= ".$data['id'].",						
                            iceshot_votos_id_pagina       	= '".$pagina."',                            
                            iceshot_votos_autor 		= 'insertVoto',
                            iceshot_votos_editor 		= 'insertVoto',
                            iceshot_votos_autor_sessid 		= '".$accessToken."',
                            iceshot_votos_editor_sessid	 	= '".$accessToken."',
                            iceshot_votos_fecha_creacion 	= '".$this->getTime()."',
                            iceshot_votos_fecha_modificacion	= '".$this->getTime()."'";
                $sqlConect->execute($sql);
                echo 'true';//$sql;
                //$this->comentarios($accessToken,$pagina);
            }
            else
            {
                echo 'false';
            }
        }
        
        
        //////////////////////////////////////
        /**
	 * Devuelve el código del formulario
	 */ 
	public function showFormulario($accessToken)
	{
            $data['#accessToken#'] 	= $accessToken;
            
            $tpl = $this->getTemplate('tpl_formulario.html');
            $tpl = str_replace(array_keys($data),array_values($data),$tpl);
            echo $tpl;
	} # eof
	
        /*
         * Genera el thumb de la imagen guardando la imagen en assets/cache
         */
        public function uploadThumb()
        {
            if( isset($_FILES["file"]) /*&& $_FILES['file']['size']<(400*1024)
				    && ($_FILES['file']['type']=='image/jpeg' || $_FILES['file']['type']=='image/pjpeg')*/)
            {
                    $fileName = $_FILES['file']['name'];
                    $extTmp = '.'.pathinfo($fileName, PATHINFO_EXTENSION);
                    
                    $nombre = 'thumb'.rand(0,100).'-'.rand(0,100).$extTmp;
                    $_SESSION['thumb'] = $nombre;
                    // File object 
                    $file = $_FILES['file'];   
                    
                    if ($file["error"] > 0)
                    {
                            echo "Return Code: " . $file["error"] . "<br />";
                    }
                    else
                    {
                            // TODO: resize image
                            // Save image in uploads folder
                            $path = 'assets/cache/' .$nombre;
                            move_uploaded_file($file["tmp_name"], $path);        
                            echo 'assets/cache/'.$nombre;
                    }
                        
                    
            }
            else
            {
                    echo 'false';
                    //print_r($_FILES);
            }
        }
        
        
        /*
         * Guarda los datos del formulario en la BD
         */
        public function sendForm($accessToken)
        {
            require_once 'assets/libraries/ResizeImage.php';
            
            if(	isset($_FILES["file"])
                    /*&& $_FILES['file']['size']<(400*1024)
                    && ($_FILES['file']['type']=='image/jpeg' || $_FILES['file']['type']=='image/pjpeg')*/)
            {
                    if ($_FILES["file"]["error"] > 0)
                    {
                            echo "false";
                    }
                    else
                    {
                            $sqlConect 	= new Sql();
                            $sqlConect->connect();
                            
                            $output_dir 	= "assets/cache/";
                            
                            if($_REQUEST['x1']!='')
                            {
                                    list($width, $height, $type, $attr) = getimagesize($_FILES["file"]['tmp_name']);
                                    
                                    $sql 	= "SELECT max(core_cloudfiles_id) as max
                                                    FROM core_cloudfiles ";
                                    
                                    $result 	= $sqlConect->execute($sql);
                                    $row	= $result->fetch_assoc();
                                    $idFile     = (int) $row['max'];
                                    $idFile++;
                                    
                                    $fileName = $_FILES['file']['name'];
                                    $fileSize = $_FILES['file']['size'];
                                    $fileType = $_FILES['file']['type'];
                                    $fileExt  = '.'.pathinfo($fileName, PATHINFO_EXTENSION);
                                                                                            
                                    $archivoCache 	= md5('core_cloudfiles'.$idFile);
                                    $image_width 	= $_REQUEST['w']; //tamaño selección
                                    $image_height 	= $_REQUEST['h'];
                                    $image_width1 	= $_REQUEST['w1']; //tamaño tab
                                    $image_height1 	= $_REQUEST['h1'];
                                    $source_x	        = ($_REQUEST['x1'] * $width) / $image_width1;
                                    $source_y	        = ($_REQUEST['y1'] * $width) / $image_width1;
                                    $source_x1	        = ($_REQUEST['x2'] * $width) / $image_width1;
                                    $source_y1	        = ($_REQUEST['y2'] * $width) / $image_width1;
                                    $image_width 	= round($source_x1 - $source_x);//265;//tamaño final
                                    $image_height 	= round($source_y1 - $source_y);//265;//tamaño final
                                    
                                    
                                    
                                    $extTmp = pathinfo($fileName, PATHINFO_EXTENSION);
                                    if($extTmp == 'jpg' || $extTmp == 'JPG' )
                                    {
                                            $src  		= imagecreatefromjpeg($_FILES['file']['tmp_name']);
                                            $dest 		= imagecreatetruecolor($image_width, $image_height);
                                            
                                            imagecopy($dest, $src, 0, 0, $source_x, $source_y, $image_width, $image_height);
                                            imagejpeg($dest, $output_dir. $archivoCache.$fileExt);								      imagedestroy($dest);
                                            imagedestroy($src);
                                            
                                            $resize = new ResizeImage($output_dir. $archivoCache.$fileExt);
                                            $resize->resizeTo(265, 265, 'exact');
                                            $resize->saveImage($output_dir. $archivoCache.$fileExt);
                                    }
                                    
                                    if($extTmp == 'png' || $extTmp == 'PNG' )
                                    {
                                            $src  		= imagecreatefrompng($_FILES['file']['tmp_name']);
                                            $dest 		= imagecreatetruecolor($image_width, $image_height);
                                    
                                            imagecopy($dest, $src, 0, 0, $source_x, $source_y, $image_width, $image_height);
                                            imagepng($dest, $output_dir. $archivoCache.$fileExt);
                                            imagedestroy($dest);
                                            imagedestroy($src);
                                            
                                            $resize = new ResizeImage($output_dir. $archivoCache.$fileExt);
                                            $resize->resizeTo(265, 265, 'exact');
                                            $resize->saveImage($output_dir. $archivoCache.$fileExt);
                                    }
                                    
                                    if($extTmp == 'gif' || $extTmp == 'GIF')
                                    {
                                            $src  		= imagecreatefromgif($_FILES['file']['tmp_name']);
                                            $dest 		= imagecreatetruecolor($image_width, $image_height);
                                    
                                            imagecopy($dest, $src, 0, 0, $source_x, $source_y, $image_width, $image_height);
                                            imagegif($dest, $output_dir. $archivoCache.$fileExt);
                                            imagedestroy($dest);
                                            imagedestroy($src);
                                            
                                            $resize = new ResizeImage($output_dir. $archivoCache.$fileExt);
                                            $resize->resizeTo(265, 265, 'exact');
                                            $resize->saveImage($output_dir. $archivoCache.$fileExt);
                                    }
                                    
                                    
                                    //move_uploaded_file($_FILES["file"]["tmp_name"],$output_dir. $archivoCache.'.jpg');
                                    
                                    $data = file_get_contents( $output_dir. $archivoCache.$fileExt);
                                    $data = $sqlConect->realEscapeString($data);
                                    
                                    $sql = "INSERT core_cloudfiles
                                                    SET
                                                            core_cloudfiles_filename		='$archivoCache',
                                                            core_cloudfiles_size 		='$fileSize',
                                                            core_cloudfiles_extension		='$fileExt',
                                                            core_cloudfiles_content		='$data',
                                                            core_cloudfiles_content_type	='$fileType',
                                                            core_cloudfiles_autor 		= 'sendForm',
                                                            core_cloudfiles_editor 		= 'sendForm',
                                                            core_cloudfiles_autor_sessid 	= '".$accessToken."',
                                                            core_cloudfiles_editor_sessid	= '".$accessToken."',
                                                            core_cloudfiles_fecha_creacion 	= '".$this->getTime()."',
                                                            core_cloudfiles_fecha_modificacion	= '".$this->getTime()."'
                                           ";
                                   // echo $sql;
                                    $sqlConect->execute($sql);
                                    $idFile = $sqlConect->getID();
                            }
                            else
                            {
                                    $sql    = "SELECT max(core_cloudfiles_id) as max
                                                    FROM core_cloudfiles ";
                                    $result 	= $sqlConect->execute($sql);
                                    $row 		= $result->fetch_assoc();
                                    $idFile         = (int) $row['max'];
                                    $idFile++;
                                    
                                    $archivoCache 	= md5('core_cloudfiles'.$idFile);
                                    
                                    $data = file_get_contents( $_FILES['file']['tmp_name']);
                                    $data = $sqlConect->realEscapeString($data);
                                    
                                    $fileName = $_FILES['file']['name'];
                                    $fileSize = $_FILES['file']['size'];
                                    $fileType = $_FILES['file']['type'];
                                    //$fileExt  = substr($fileName, strrpos($fileName, '.'));
                                    $fileExt = '.'.pathinfo($fileName, PATHINFO_EXTENSION);
                                    
                                    $sql = "INSERT core_cloudfiles
                                                    SET
                                                            core_cloudfiles_filename		='$archivoCache',
                                                            core_cloudfiles_size 		='$fileSize',
                                                            core_cloudfiles_extension		='$fileExt',
                                                            core_cloudfiles_content		='$data',
                                                            core_cloudfiles_content_type	='$fileType',
                                                            core_cloudfiles_autor 		= 'CMS.php->uploadPhoto',
                                                            core_cloudfiles_editor 		= 'CMS.php->uploadPhoto',
                                                            core_cloudfiles_autor_sessid	= '".session_id()."',
                                                            core_cloudfiles_editor_sessid	= '".session_id()."',
                                                            core_cloudfiles_fecha_creacion 	= '".$this->getTime()."',
                                                            core_cloudfiles_fecha_modificacion	= '".$this->getTime()."'
                                           ";
                                    //echo $sql;
                                    $sqlConect->execute($sql);
                                    $idFile = $sqlConect->getID();
                                    
                                    $archivoCache 	= md5('core_cloudfiles'.$idFile);
                                    move_uploaded_file($_FILES["file"]["tmp_name"],$output_dir. $archivoCache.$fileExt);
                            }
                            
                            
                            
                            
                            
                            
                            
                            
                            $data   = $this->getUserData($accessToken);
                            $nombre = $_REQUEST['nombre'];
                            
                            $sql = "INSERT iceshot_formulario
                                            SET
                                                iceshot_formulario_id_facebook			= ".$data['id'].",
                                                iceshot_formulario_id_image 			= $idFile,
                                                iceshot_formulario_nombre			= '".$nombre."',
                                                iceshot_formulario_autor 			= 'sendForm',
                                                iceshot_formulario_editor 			= 'sendForm',
                                                iceshot_formulario_autor_sessid 		= '".$accessToken."',
                                                iceshot_formulario_editor_sessid		= '".$accessToken."',
                                                iceshot_formulario_fecha_creacion 		= '".$this->getTime()."',
                                                iceshot_formulario_fecha_modificacion		= '".$this->getTime()."'
                                   ";
                            //echo $sql;

							mysql_real_escape_string($sql);
                            $sqlConect->execute($sql);
                            
                            echo "ok";
                    }
             
            }
            else
            {
                    echo 'false';
            }
        }
        
        //------------------------------------------------------------------------
	//------------------------------------------------------------------------	
	// Funciones comúnes para todas las apps, no editar!	
	//------------------------------------------------------------------------
	//------------------------------------------------------------------------
    
	/**
	 * Regresa el timestamp
	 */
	public function getTime()
	{
		$fecha = new DateTime('NOW', new DateTimeZone('America/Mexico_City'));
		$now = $fecha->format('Y-m-d H:i:s');
		
		return $now;
	}
	
        /*
         * Regresa hace cuanto fue escrito el comentario.
         */
        function time_delta($timestamp)
	{
		$fechaActual = new DateTime('NOW', new DateTimeZone('America/Mexico_City'));
		$fechaBD = new DateTime($timestamp, new DateTimeZone('America/Mexico_City'));
		
		$interval = $fechaBD->diff($fechaActual);
		//$interval->format('%R%i minutos');
		
		$meses    = $interval->format("%m");
		$dias     = $interval->format("%d");
		$horas    = $interval->format("%H");
		$minutos  = $interval->format("%i");
		$segundos = $interval->format("%s");
		
		$tiempo='';
		
		if($meses != '0')
		{
			if($meses != '1')
				$tiempo.=$meses.' meses ';
			else
				$tiempo.=$meses.' mes ';
		}
		else		
			if($dias != '0')
			{
				if($dias != '1')
					$tiempo.=$dias.' días ';
				else
					$tiempo.=$dias.' día ';
			}
			else
		
				if($horas != '0')
				{
					if($horas != '1')
						$tiempo.=$horas.' horas ';
					else
						$tiempo.=$horas.' hora ';
				}
				else
		
					if($minutos != '0')
					{
						if($minutos != '1')
							$tiempo.=$minutos.' minutos ';
						else
							$tiempo.=$minutos.' minuto ';
					}
					else
		
						if($segundos != '')
						{
							if($minutos != '1')
								$tiempo.=$segundos.' segundos';
							else
								$tiempo.=$segundos.' segundo';
						}
		
		return $tiempo;
		
	}
        
	/**
	 * Redirecciona al url del login
	 */
	public function getLogin()
	{
            //https://www.facebook.com/TiaRosaMx/app_524833827602178'
	    $loginUrl = $this->facebook->getLoginUrl
            (
                array
                (
                    'scope' => $this->scope,
                    'redirect_uri' => 'https://www.facebook.com/'.$this->fanpage.'/app_'.$this->app_id,
                )
            );
    
        return '<script> top.location.href=\'' . $loginUrl . '\'</script>';
    
	} # eof
	
	/**
	 * Devuelve el accesToken de la sesión de facebook
	 */
	public function getAccessToken()
	{
	    $at = '';
	    
	    try
	    {
		$at = $this->facebook->getAccessToken();
	    }
	    catch(FacebookApiException $e)
	    {
		//echo $e->getType().'<br>';
		//echo $e->getMessage().'<br>';
	    }
		
	    return $at;
	} # eof
	
	/**
	 * Devuelve el accesToken de la sesión de facebook
	 */
	public function setExtendedAccessToken()
	{
	    return $this->facebook->setExtendedAccessToken();
	} # eof
	
	/**
	 * Devuelve la información del usuario
	 */
	public function getUserID($accessToken)
	{
	    $data = '';
	    $this->facebook->setAccessToken($accessToken);
	    
	    try
	    {
		$data = $this->facebook->getUser();
	    }
	    catch(FacebookApiException $e)
	    {
		echo $e->getType().'<br>';
		echo $e->getMessage().'<br>';
	    }
		
	    return $data;
	} # eof
	
	/**
	 * Devuelve la información del usuario
	 */
	public function getUserData($accessToken)
	{
	    $data = '';
	    
	    try
	    {
		$this->facebook->setAccessToken($accessToken);		
		$data = $this->facebook->api('/me');
	    }
	    catch(FacebookApiException $e)
	    {
		//echo $e->getType().'<br>';
		//echo $e->getMessage().'<br>';
	    }
		
	    return $data;
		
	} # eof
	
	/*
	 * Devuelve la información pública de algun usuario por id de FB
	 */
	public function getDataId($id)
	{
	    return $this->facebook->api('/'.$id,'GET');
	}
	
	
	/**
	 * Devuelve la información del usuario
	 */
	public function getFriendsUserName($accessToken)
	{
		$this->facebook->setAccessToken($accessToken);
	    
	    return $this->facebook->api('/me/friends');
	} # eof
	
	/**
	 * Devuelve un arreglo con la imagen, nombre e id de tus amigos.
	 */
	public function getFriendsPics($accessToken,$limit,$pagina)
	{
		$this->facebook->setAccessToken($accessToken);
	    
		$friends = $this->facebook->api('/me/friends?limit='.$limit.'&offset='.$pagina);
		
		$data		= array();
		
		foreach ($friends['data'] as $friend)
		{
			array_push($data, array('https://graph.facebook.com/' . $friend['id'] . '/picture', $friend['name'], $friend['id']));
		}
		
		return $data;
	    
	} # eof
	
	/**
	 * Devuelve un arreglo con la imagen, nombre e id de tus amigos.
	 */
	public function getFriendsList($accessToken)
	{
		$this->facebook->setAccessToken($accessToken);
	    
		//$friends = $this->facebook->api('/me/friends?limit=20');
		$friends 	= $this->facebook->api('/me/friends');
		$data		= array();
		
		foreach ($friends['data'] as $friend)
		{
			array_push($data, array('https://graph.facebook.com/' . $friend['id'] . '/picture',$friend['name'], $friend['id']));
		}
		
		return $data;
	    
	} # eof
	
	/**
	 * Devuelve el URL de la foto de perfil del usuario arctivo
	 *
	 *  tamaños: 	square 	  (50x50),
	 *  small 	  (50 pixels wide, variable height),
	 *  normal 	  (100 pixels wide, variable height),
	 *  and large (about 200 pixels wide, variable height):
	 */
	public function getProfilePic($accessToken, $size = '')
	{
	    if($size!= '') $size = '?type=' . $size;
	    
	    $profilePicUrl =  "https://graph.facebook.com/" . $this->getUserID($accessToken) . "/picture" . $size;
	  
		return $profilePicUrl;
		
	} # eof
	
	
	/**
	 * Obtiene el contenido de un archivo de plantilla definido en 'assets/templates'
	 */
	public function getTemplate($src)
	{
            $ruta = '../templates/';
            $tpl = '';
            
            if(file_exists($ruta . $src))
            {
                    $tpl = file_get_contents($ruta .$src);
                    
                    return $tpl;
            }
            else
            {
                    return 'no existe';
            }
	} # eof
	
	/** 
	 * Guarda el access token en la bd en caso de que no exista
	 */
	public function saveAccessToken($accessToken)
	{	
		$sqlConect 	= new Sql();		
		$sqlConect->connect();
		
		$accessToken	= $this->getAccessToken();
		$facebookId	= $this->getUserID($accessToken);	
		
		$url 		= "https://graph.facebook.com/oauth/access_token?client_id=" . 
                                    $this->app_id . "&client_secret=" . 
                                    $this->app_secret . "&grant_type=fb_exchange_token&fb_exchange_token=" . $accessToken;
			
		$result 	= file_get_contents($url);
		$result		= explode('=', $result);
		$result		= explode('&', $result[1]);
		$eAccessToken	= $result[0];
		
		$sql 		= "SELECT count(*) as total
					FROM iceshot_tokens
					WHERE iceshot_tokens_facebook_id = '" . $facebookId . "'";
					
		$result 	= $sqlConect->execute($sql);		
		$row 		= $result->fetch_assoc();
		
		if($row['total']==0)
		{	
			$sqlI 	= "INSERT iceshot_tokens
					SET iceshot_tokens_facebook_id 	        = '" . $facebookId . "',
					    iceshot_tokens_access_token 	= '" . $eAccessToken . "',
					    iceshot_tokens_fecha_creacion	= '" . $this->getTime() . "',
					    iceshot_tokens_fecha_modificacion= '" . $this->getTime() . "'";
			
			echo 	$sqlI;		
			$sqlConect->execute($sqlI);
						
		}
		else
		{
			$sqlU 	= "UPDATE iceshot_tokens
					SET iceshot_tokens_access_token 	= '" . $eAccessToken . "',
					    iceshot_tokens_fecha_creacion	= '" . $this->getTime() . "',
					    iceshot_tokens_fecha_modificacion= '" . $this->getTime() . "'
					WHERE iceshot_tokens_facebook_id 	=  " . $facebookId;
			
			echo 	$sqlU;		
			$sqlConect->execute($sqlU);
		}
	
	} # eof
	
	/*
	 * Guarda el user agent 
	 */
	public function saveUseragent($accessToken,$userID)
	{
	    $user_id 		= $userID;//$this->facebook->getUser();
	    
	    $sqlConect = new Sql();
	    $sqlConect->connect();
	   
	    if($user_id)
	    {
		$accessToken = $this->getAccessToken();
		$data   = $this->getUserData($accessToken);
		
		$sql = "INSERT iceshot_useragent
					SET iceshot_useragent_id_facebook		= '".$user_id."',
					    iceshot_useragent_mail			= '',
					    iceshot_useragent_useragent			= '".$_SERVER['HTTP_USER_AGENT']."',
					    iceshot_useragent_fecha_creacion 		= '".$this->getTime()."',
					    iceshot_useragent_fecha_modificacion	= '".$this->getTime()."'
			       ";	
		echo $sql;
		$sqlConect->execute($sql);

		//$this->saveUserdataFB($accessToken);
	    }
	    else
	    {
		$sql = "INSERT iceshot_useragent
					SET iceshot_useragent_id_facebook		= 0,
					    iceshot_useragent_mail			= 'Not logged',
					    iceshot_useragent_useragent			= '".$_SERVER['HTTP_USER_AGENT']."',
					    iceshot_useragent_fecha_creacion 		= '".$this->getTime()."',
					    iceshot_useragent_fecha_modificacion	= '".$this->getTime()."'
			       ";	
		echo $sql;
		$sqlConect->execute($sql);
	    }
		
		
	}
     
	/*
     * Guarda todos los datos del objeto del usuario de FB
     */
    public function saveUserdataFB($data)
    {
	echo 	'saveUserdataFB';
	print_r($data);			    
	
	
	
    	$sqlConect = new Sql();
        $sqlConect->connect();
            
        //$data   = $this->getUserData($accessToken);
            
        $sql 		= "SELECT iceshot_userfb_id
        			   FROM iceshot_userfb
				    WHERE iceshot_userfb_id_fb = '".$data['id']."'";
	
	echo $sql;
                                    
	$result = $sqlConect->execute($sql);		
        $row    = $result->fetch_assoc();
            
        if($row['iceshot_userfb_id']=='')
        {
        	//$fecha = new DateTime($data['birthday']);
		//$fecha = $fecha->format('Y-m-d');
		//iceshot_userfb_birthday	        = '".$fecha."',
		    
		$sql = "INSERT iceshot_userfb   
			    SET iceshot_userfb_id_fb	= '".$data['id']."',					    
			iceshot_userfb_email		= '".$data['email']."',
			iceshot_userfb_name		= '".$data['name']."',
			iceshot_userfb_gender		= '".$data['gender']."',
			iceshot_userfb_hometown		= '".$data['locale']."',
			iceshot_userfb_fecha_creacion 	= '".$this->getTime()."',
			iceshot_userfb_fecha_modificacion	= '".$this->getTime()."'
			";
		echo $sql;
                           
		$sqlConect->execute($sql);    
     	}
                 
    } # EOF
   
     /*
	 * Guarda la actividad
	 */
	public function saveActivity($accessToken,$actividad)
	{
	    $user_id 		= $this->facebook->getUser();
	    
	    $sqlConect 		= new Sql();
	    $sqlConect->connect();
	       
	    if($user_id)
	    {
		$accessToken = $this->getAccessToken();
		$data   = $this->getUserData($accessToken);
		
		$sql = "INSERT iceshot_actividades
					SET iceshot_actividades_id_facebook		= '".$data['id']."',
					    iceshot_actividades_actividad		= '".$actividad."',
					    iceshot_actividades_session			= '".$accessToken."',
					    iceshot_actividades_fecha_creacion 		= '".$this->getTime()."',
					    iceshot_actividades_fecha_modificacion	= '".$this->getTime()."'
			       ";	
		//echo $sql;
		$sqlConect->execute($sql);
	    }
	    else
	    {
		$sql = "INSERT iceshot_actividades
					SET iceshot_actividades_id_facebook		= 0,
					    iceshot_actividades_actividad		= '".$actividad."',
					    iceshot_actividades_session			= '".$accessToken."',
					    iceshot_actividades_fecha_creacion 		= '".$this->getTime()."',
					    iceshot_actividades_fecha_modificacion	= '".$this->getTime()."'
			       ";	
		//echo $sql;
		$sqlConect->execute($sql);
	    }
		
	}
	
        public function get_bitly_short_url($url,$format='txt')
        {
            $connectURL = 'http://api.bit.ly/v3/shorten?login=yaotzin68&apiKey=R_7965d86312164532bd773c938de33166&uri='.urlencode($url).'&format='.$format;
            return $this->curl_get_result($connectURL);
        }   
        
        /* returns a result form url */
        private function curl_get_result($url)
        {
                $ch = curl_init();
                $timeout = 5;
                curl_setopt($ch,CURLOPT_URL,$url);
                curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
                curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,$timeout);
                $data = curl_exec($ch);
                curl_close($ch);
                return $data;
        }
	
        /*
         * Postea en FB una foto
         */
        public function postPhoto($photoMsg,$photoUrl,$tag)
	{	
	    /*$album_details = array(
		'message'=> $albumDesc,
		'name'=> $albumName
	    );
	   
	    $create_album = $this->facebook->api('/me/albums', 'post', $album_details);
	    $album_uid = $create_album['id'];*/
        
	    $file = $photoUrl;
	    $photo_details = array
            (
		'message'=> $photoMsg,
	    );
	    $photo_details[basename($file)] = '@' . realpath($file);
	    
	    try
	    {  
		    $this->facebook->setFileUploadSupport(true);
		    $upload_photo = $this->facebook->api('/me/photos', 'post', $photo_details);
	    }
	    catch (FacebookApiException $e) 
	    {
		    echo 'Could not post image to Facebook. '.$e;
	    }
	    echo '<br> upload id: '.$upload_photo['id'];
	    
	    $photo_id = $upload_photo['id'];
	    
	    $tag_params = array(
	        'to'       => $tag,
		'x'        => 50,
		'y'        => 0
	    );
        
	try
	{ 
	    $this->facebook->api('/'.$photo_id.'/tags', 'post', $tag_params);
	}
	catch (FacebookApiException $e) 
	{
	    echo 'Could not tag image to Facebook. '.$e;
	}
    } # END of function
        
        /**
	 * Ejecuta la llamada a varias funciones internas para verificar las llamadas a la api de facebook
	 */
	public function test($accessToken)
	{
		echo '<h2>$app->getUserId(): ' . $this->getUserID($accessToken). '</h2>';
		
		echo '<h2>$app->getUserProfilePic()</h2>';
		echo '<img src="' . $this->getProfilePic($accessToken) . '" />';
		
	
		echo '<h2>$app->getUserData()</h2>';
		echo '<pre>' . print_r($this->getUserData($accessToken), 1) . '</pre>';
		
		echo '<h2>$app->getFriendsUserName()</h2>';		
		echo '<pre>' . print_r($this->getFriendsUserName($accessToken), 1) . '</pre>';	
		
		echo '<h2>$app->getFriendsPics()</h2>';		
		echo '<pre>' . print_r($this->getFriendsPics($accessToken), 1) . '</pre>';
		
		echo '<h2>$app->displayFriendsPics()</h2>';
		$this->displayFriendsPics($accessToken);
	} # eof
	
	/**
	* Parses a user agent string into its important parts
	*
	* @author Jesse G. Donat <donatj@gmail.com>
	* @link https://github.com/donatj/PhpUserAgent
	* @link http://donatstudios.com/PHP-Parser-HTTP_USER_AGENT
	* @param string|null $u_agent
	* @return array an array with browser, version and platform keys
	*/
	public function parse_user_agent( $u_agent = null )
	{
	   if( is_null($u_agent) && isset($_SERVER['HTTP_USER_AGENT']) ) $u_agent = $_SERVER['HTTP_USER_AGENT'];
       
	   $platform = null;
	   $browser  = null;
	   $version  = null;
       
	   $empty = array( 'platform' => $platform, 'browser' => $browser, 'version' => $version );
       
	   if( !$u_agent ) return $empty;
       
	   if( preg_match('/\((.*?)\)/im', $u_agent, $parent_matches) ) {
       
	       preg_match_all('/(?P<platform>Android|CrOS|iPhone|iPad|Linux|Macintosh|Windows(\ Phone\ OS)?|Silk|linux-gnu|BlackBerry|PlayBook|Nintendo\ (WiiU?|3DS)|Xbox)
		   (?:\ [^;]*)?
		   (?:;|$)/imx', $parent_matches[1], $result, PREG_PATTERN_ORDER);
       
	       $priority           = array( 'Android', 'Xbox' );
	       $result['platform'] = array_unique($result['platform']);
	       if( count($result['platform']) > 1 ) {
		   if( $keys = array_intersect($priority, $result['platform']) ) {
		       $platform = reset($keys);
		   } else {
		       $platform = $result['platform'][0];
		   }
	       } elseif( isset($result['platform'][0]) ) {
		   $platform = $result['platform'][0];
	       }
	   }
       
	   if( $platform == 'linux-gnu' ) {
	       $platform = 'Linux';
	   } elseif( $platform == 'CrOS' ) {
	       $platform = 'Chrome OS';
	   }
       
	   preg_match_all('%(?P<browser>Camino|Kindle(\ Fire\ Build)?|Firefox|Iceweasel|Safari|MSIE|Trident/.*rv|AppleWebKit|Chrome|IEMobile|Opera|OPR|Silk|Lynx|Midori|Version|Wget|curl|NintendoBrowser|PLAYSTATION\ (\d|Vita)+)
		   (?:\)?;?)
		   (?:(?:[:/ ])(?P<version>[0-9A-Z.]+)|/(?:[A-Z]*))%ix',
	       $u_agent, $result, PREG_PATTERN_ORDER);
       
       
	   // If nothing matched, return null (to avoid undefined index errors)
	   if( !isset($result['browser'][0]) || !isset($result['version'][0]) ) {
	       return $empty;
	   }
       
	   $browser = $result['browser'][0];
	   $version = $result['version'][0];
       
	   $find = function ( $search, &$key ) use ( $result ) {
	       $xkey = array_search(strtolower($search),array_map('strtolower',$result['browser']));
	       if( $xkey !== false ) {
		   $key = $xkey;
       
		   return true;
	       }
       
	       return false;
	   };
       
	   $key = 0;
	   if( $browser == 'Iceweasel' ) {
	       $browser = 'Firefox';
	   }elseif( $find('Playstation Vita', $key) ) {
	       $platform = 'PlayStation Vita';
	       $browser  = 'Browser';
	   } elseif( $find('Kindle Fire Build', $key) || $find('Silk', $key) ) {
	       $browser  = $result['browser'][$key] == 'Silk' ? 'Silk' : 'Kindle';
	       $platform = 'Kindle Fire';
	       if( !($version = $result['version'][$key]) || !is_numeric($version[0]) ) {
		   $version = $result['version'][array_search('Version', $result['browser'])];
	       }
	   } elseif( $find('NintendoBrowser', $key) || $platform == 'Nintendo 3DS' ) {
	       $browser = 'NintendoBrowser';
	       $version = $result['version'][$key];
	   } elseif( $find('Kindle', $key) ) {
	       $browser  = $result['browser'][$key];
	       $platform = 'Kindle';
	       $version  = $result['version'][$key];
	   } elseif( $find('OPR', $key) ) {
	       $browser = 'Opera Next';
	       $version = $result['version'][$key];
	   } elseif( $find('Opera', $key) ) {
	       $browser = 'Opera';
	       $find('Version', $key);
	       $version = $result['version'][$key];
	   } elseif( $find('Midori', $key) ) {
	       $browser = 'Midori';
	       $version = $result['version'][$key]; 
	   } elseif( $browser == 'AppleWebKit' ) {
	       if( ($platform == 'Android' && !($key = 0)) || $find('Chrome', $key) ) {
		   $browser = 'Chrome';
	       } elseif( $platform == 'BlackBerry' || $platform == 'PlayBook' ) {
		   $browser = 'BlackBerry Browser';
	       } elseif( $find('Safari', $key) ) {
		   $browser = 'Safari';
	       }
       
	       $find('Version', $key);
       
	       $version = $result['version'][$key];
	   } elseif( $browser == 'MSIE' || strpos($browser, 'Trident') !== false ) {
	       if( $find('IEMobile', $key) ) {
		   $browser = 'IEMobile';
	       } else {
		   $browser = 'MSIE';
		   $key     = 0;
	       }
	       $version = $result['version'][$key];
	   } elseif( $key = array_search('playstation 3', array_map('strtolower', $result['browser'])) === 0 ) {
	       $platform = 'PlayStation 3';
	       $browser  = 'NetFront';
	   }
       
	   return array( 'platform' => $platform, 'browser' => $browser, 'version' => $version );
       
       }
    
} # End of class
?>