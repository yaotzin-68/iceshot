<?php

	/** Comentar en producción */
	error_reporting(E_ALL);
	ini_set('display_errors', '1');
	/** Comentar en producción */

	header('P3P:CP="IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT"');
		
	require_once('app.php');

	if((isset($_REQUEST['action']) && $_REQUEST['action'] != '') &&
	   (isset($_REQUEST['accessToken']) && $_REQUEST['accessToken'] != ''))
	{
		$action 	= $_REQUEST['action'];
		$accessToken 	= $_REQUEST['accessToken'];
	
		$app = new App();
		
		if($action!='saveUseragent' && $action!='getFBID' && $action!='getFBID')
		{
			//$app->saveActivity($accessToken,$action);
		}
	
		switch($action)
		{
			case 'saveUseragent':	
				$app->saveUseragent($accessToken,$_REQUEST['userID']);
			break;
		
			case 'saveUserFB':	
				$app->saveUserdataFB($_REQUEST['data']);
			break;
		
			case 'getFBID':	
				$app->getFBID();
			break;
		
			case 'getVideos':	
				$app->getVideos();
			break;
		
			case 'getComentariosVideo':
				$app->getComentariosVideo($_REQUEST['idVideo'],$_REQUEST['userID']);
			break;
		
			case 'insertComentarios':
				$app->insertComentarios($accessToken,$_REQUEST['pagina'],$_REQUEST['comentario'],$_REQUEST['userID']);								
			break;
		
			case 'getShortUrl':
				$app->getShortUrl($_REQUEST['url']);								
			break;
		
			case 'doLoginFB':
				$app->doLoginFB();								
			break;
			
			case 'loadListAmigos':	
				$app->loadListAmigos($accessToken);
			break;
		
			case 'etiquetarAmigo':
				$app->etiquetarAmigo($accessToken,$_REQUEST['imagen'],$_REQUEST['background'],$_REQUEST['amigoId']);
			break;
		
			case 'getHome':		
				$app->getHome($accessToken);								
			break;
		
			case 'getYoutubeVideo':	
				$app->getYoutubeVideo($accessToken);								
			break;
		
			case 'facebookComments':
				$app->facebookComments($accessToken);								
			break;
		
			case 'insertComentariosPlugin':
				$app->insertComentariosPlugin($accessToken);								
			break;
		
			case 'comentarios':
				$app->comentarios($accessToken,$_REQUEST['pagina']);								
			break;
		
			
		
			case 'insertVoto':
				$app->insertVoto($accessToken,$_REQUEST['pagina']);								
			break;
		
			case 'showFormulario':
				$app->showFormulario($accessToken);								
			break;
		
			case 'uploadThumb':
				$app->uploadThumb();
			break;
		
			case 'sendForm':
				$app->sendForm($accessToken);
			break;
		
			case 'test':
				$app->test($accessToken);
			break;
		
		
		
		
		
			
			
			case 'cargarAmigos':
				$app = new App();
				$app->cargarAmigos($accessToken,$_REQUEST['pagina']);								
			break;
			
			case 'getHome':					
				$app = new App();
				$app->getHome($accessToken);								
			break;
		
			case 'getGaleria':					
				$app = new App();
				$app->getGaleria($accessToken,$_REQUEST['pagina']);								
			break;
				
			case 'getMotivos':					
				$app = new App();
				$app->getMotivos($accessToken, $_REQUEST['idFrase']);
			break;
		
			case 'insertDedicatoria':					
				$app = new App();
				$app->insertDedicatoria($accessToken,$_REQUEST['idFrase'],$_REQUEST['amigo'],$_REQUEST['ganador']);								
			break;
		
			case 'getGanadores':
				$app = new App();
				$app->getGanadores($accessToken,$_REQUEST['pagina']);
			break;
		
			case 'getFraseGanadora':
				$app = new App();
				$app->getFraseGanadora($accessToken, $_REQUEST['idFrase']);
			break;
		
			case 'getAviso':
				$app = new App();
				$app->getAviso($accessToken);
			break;
		
			case 'getTerminos':
				$app = new App();
				$app->getTerminos($accessToken);
			break;
		
			case 'getPoliticas':
				$app = new App();
				$app->getPoliticas($accessToken);
			break;
		
			case 'postFrases':
				$app = new App();
				$app->postFrases($accessToken,$_REQUEST['mensaje']);
			break;
				
				
		}		
	}		
?>