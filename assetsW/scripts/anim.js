(function ($, window) {

    'use strict';

    $(document).ready(function () {

        var deleteWhite = function () {
            $('.white-overlay').remove();
        };

        if( $('html').hasClass('ie8') ) {

            deleteWhite();

        } else {
            window.setTimeout(function () {

                var iceTL = new TimelineMax();

                iceTL.to( $('.white-overlay'), 0.25, {opacity: 0, ease: Quart.easeOut, onComplete: deleteWhite} );

                iceTL.from( $('#doc .logo'), 0.25, {x: -250, opacity: 0, ease: Quart.easeOut} );
                iceTL.from( $('#doc .homeTitulo'), 0.25, {x: 250, opacity: 0, ease: Quart.easeOut} );
                iceTL.from( $('#doc .homeCallAction'), 0.25, {x: 250, opacity: 0, ease: Quart.easeOut} );
                iceTL.from( $('#doc #comentarios').parent() , 0.25, {x: -250, opacity: 0, ease: Quart.easeOut} );

                iceTL.from( $('#doc .ftTerminos').parent() , 0.25, {y: 250, opacity: 0, ease: Quart.easeOut}, 'legales' );
                iceTL.from( $('#doc .ftAviso').parent() , 0.25, {y: 250, opacity: 0, ease: Quart.easeOut}, 'legales' );
                iceTL.from( $('#doc .ftShare').parent() , 0.25, {y: 250, opacity: 0, ease: Quart.easeOut}, 'legales' );

                iceTL.stop();

                window.setTimeout(function () {

                    iceTL.play();

                    $('.comentarioBtn').hover(
                        function () {

                            TweenMax.to( $(this), 0.25, {scale: 0.9, ease: Quart.easeOut} );
                        },
                        function () {

                            TweenMax.to( $(this), 0.25, {scale: 1, ease: Quart.easeOut} );
                        }
                    );

                    $('.carruselNext, .carruselPrev').hover(
                        function () {

                            TweenMax.to( $(this), 0.25, {scale: 0.9, ease: Quart.easeOut} );
                        },
                        function () {

                            TweenMax.to( $(this), 0.25, {scale: 1, ease: Quart.easeOut} );
                        }
                    );

                }, 1500);

            }, 1250);
        }

    });

})(jQuery, window, undefined);