var DEBUG  	= false;

$(document).ready(function()
{
	if(DEBUG) console.log('...DOM ready!');
	
	Site.init();
});

function consoleLog(mensaje)
{
	if(DEBUG)
		console.log(mensaje);
}

/**
 * Declaración de la Site
 * http://www.facebook.com/dialog/pagetab?app_id=YOUR_APP_ID&next=YOUR_URL
 */
var Site = new function()
{
	this.intfc		= 'assetsW/php/intfc.php';
	this.userID 		= '';
	this.accessToken 	= '';
	this.canvasSize		= 1228;
	this.appId		= '';
	this.FB;
	this.fbPermissions	= '';
	this.videos 		= new Array();
	this.videosIndice	= 0;
	this.youtubePlayer;
	this.youtubeDone	= false;
	this.videoHash		= '';
	this.dataFB;
	
	/**
	 * Inicializa la Aplicación
	 */
	this.init = function()
	{
		if(DEBUG) console.log('...Site.init()');
		
		this._attachActionEvents();
		
		var posting = $.post(Site.intfc,
		{
			action: 'getFBID',
			accessToken: 'getFBID'
		});
		
		posting.done(function(data)
		{
			data = data.split('#')
			
			Site.appId  = data[0];
			Site.fbPermissions = data[1]
			
			//Site.validarUsuarioFB();
			Site.inicio();
		});
		
		window.onresize = function()
		{
			Site.resize();
		};
	};
	
	/*
	 * Valida si el usuario de FB ya autorizo la app sino instala la app
	 */
	this.validarUsuarioFB = function()
	{
		$.ajaxSetup({ cache: true });
		$.getScript('//connect.facebook.net/es_LA/all.js', function()
		{
			FB.init(
			{
				appId: Site.appId,
				status : true, // check login status
				cookie : true, // enable cookies to allow the server to access the session
				xfbml : true, // parse XFBML
				oauth: true
			});
			
			FB.getLoginStatus(function(response)
			{
				consoleLog(response.status);

				// Here we subscribe to the auth.authResponseChange JavaScript event. This event is fired
				// for any authentication related change, such as login, logout or session refresh. This means that
				// whenever someone who was previously logged out tries to log in again, the correct case below 
				// will be handled. 
				if (response.status === 'connected')
				{
					// the user is logged in and has authenticated your
					// app, and response.authResponse supplies
					// the user's ID, a valid access token, a signed
					// request, and the time the access token 
					// and signed request each expire
					
					Site.userID 	= response.authResponse.userID;
					Site.accessToken = response.authResponse.accessToken;
					
					consoleLog('Site.userID '+Site.userID);
					consoleLog('Site.accessToken '+Site.accessToken);
					
					Site.saveUserFB();
					//Site.inicio();
					
				}
				else if (response.status === 'not_authorized')
				{
					// the user is logged in to Facebook, 
					// but has not authenticated your app
					consoleLog('no autorizado');
					
					FB.login(function(response) 
					{
						if (response.authResponse) 
						{
							Site.userID 	= response.authResponse.userID;
							Site.accessToken = response.authResponse.accessToken;
							
							consoleLog('Site.userID '+Site.userID);
							consoleLog('Site.accessToken '+Site.accessToken);
							
							Site.inicio();
   						}
   						else
   						{
							window.location = "assets/pages/noAutorizado.html";
   						}
					},
					{
						scope: Site.fbPermissions
					});
				}
				else
				{
					// the user isn't logged in to Facebook.
					consoleLog('no loggeado a FB');
					FB.login();
				}
				
			});
			
			FB.Canvas.setSize({ height: Site.canvasSize });
			Site.FB = FB;
		});
	}
	
	this.doLoginFB = function()
	{
		var posting = $.post(Site.intfc,
		{
			action: 'doLoginFB',
			accessToken: this.accessToken
		});
		
		posting.done(function(data)
		{
			consoleLog('done: '+data);
			Site.accessToken = data;
			
			window.location.href = data;
		});
	}
	
	/**
	 * Direcciona a la sección inicial de la app (Pantalla 1)
	 */
	this.inicio = function()
	{
		FB.init(
		{
			appId: 		Site.appId,
			status : 	true, // check login status
			cookie : 	true, // enable cookies to allow the server to access the session
			xfbml : 	true, // parse XFBML
			oauth: 		true
		});
		
		FB.getLoginStatus(function(response)
		{
			consoleLog(response.status );
			
			if (response.status === 'connected')
			{
				Site.userID 		= response.authResponse.userID;
				Site.accessToken 	= response.authResponse.accessToken;
				//console.log('Logged in '+Site.accessToken);
				
				Site.saveUserFB();
			}			
		});
		
		Site.FB = FB;
		
		Site.accessToken = 'ACCESSTOKEN';
		
		var posting = $.post(Site.intfc,
		{
		    action	: 'saveUseragent',
		    accessToken	: Site.accessToken,
		    userID	: Site.userID
		});

		
		
		$('#dynamic').hide();
		
		Site.videoHash = Site._GetURLParameter('app_data');
		
		var posting = $.post(Site.intfc,
		{
			action: 'getHome',
			accessToken: this.accessToken
		});
		
		posting.done(function(data)
		{
			//console.log('gethome response');
			//console.log(data);
			
			$('#contenido').html(data);
			
			$('#contenido').imagesLoaded()
			.always( function( instance )
			{
				$('#contenido').fadeIn();
				
				Site.startPlayers();
				
				Site._attachActionEvents();
				
				
			});
			
			
		});
	};
	
	/*
	 * Guarda los datos del usuario de FB
	 */
	this.saveUserFB = function()
	{
		Site.FB.api('/me', function(response)
		{
			
			//var arr = $.map(response, function(el) { return el; });
			//console.log(arr);
			
			var posting = $.post(Site.intfc,
			{
				action: 'saveUserFB',
				accessToken: Site.accessToken,
				data: response
				
			});
		});
		
		

	}
	
	/*
	 * Obtiene los id's del json y genera los divs para los players
	 */
	this.startPlayers = function()
	{
		var posting = $.post(Site.intfc,
		{
			action: 'getVideos',
			accessToken: this.accessToken
		});
		
		posting.done(function(data)
		{
			consoleLog(data);
			Site.videos = data.split(',');
			/*Site.videos.push('Mzf9XO7mvEI');
			Site.videos.push('rUqCrc4tUhM');
			Site.videos.push('q8-aoYR-bYo');
			Site.videos.push('C6SEwTyJtpE');
			Site.videos.push('f04zDTlSt4I');*/
			
			var players = '';
			for(i=0;i<Site.videos.length;i++)
			{
				players += '<div id="player'+i+'" class="youtubeVideo" data-action="showVideo" data-param="'+Site.videos[i]+'">';
				players += '	<div class="carruselPlay"></div>';
				players += '	<div class="video"></div>';
				players += '	<img src="assetsW/img/youtubeMarco.jpg">';
				players += '</div>';
				
			}
			
			$('.youtubeCarruselMov').html(players);
			
			$('.youtubeCarruselMov').imagesLoaded()
			.always( function( instance )
			{
				Site.resize();
			});
			
			Site.startPlayer(Site.videos[Site.videosIndice]);
			Site._attachActionEvents();
		});
		
		
		
		
	}
	
	this.resize = function()
	{
		var playerYtHg 	= $('.youtubeVideo').height();
		var flechasPosY	= (playerYtHg/2) - $('.carruselPrev').height()/2;
		
		$('.carrusel').height(playerYtHg);
		
		$('.carruselPrev').css('margin-top',flechasPosY+'px');
		$('.carruselNext').css('margin-top',flechasPosY+'px');
		
		$('#player').css(
		{
			height: $('.youtubeVideo:first .video').height(),
			width:	$('.youtubeVideo:first .video').width()
		});
	}
	
	this.carruselNext = function()
	{
		$('#player').hide();
		$('#player').html('');
		$('.youtubeVideo:first').appendTo($('.youtubeCarruselMov'));
		
		//consoleLog(Site.videosIndice+ ' < '+(Site.videos.length-1));
		if (Site.videosIndice < (Site.videos.length-1))
		{
			Site.videosIndice++;
		}
		else
			Site.videosIndice=0;
		
		
		Site.startPlayer(Site.videos[Site.videosIndice]);
	}
	
	this.carruselPrev = function()
	{
		$('#player').hide();
		$('#player').html('');
		$('.youtubeVideo:last').prependTo($('.youtubeCarruselMov'));
		
		if (Site.videosIndice > 0)
		{
			Site.videosIndice--;
		}
		else
			Site.videosIndice=(Site.videos.length-1);
			
		Site.startPlayer(Site.videos[Site.videosIndice]);
	}
	
	this.getComentarios = function(id)
	{
		
		
		//consoleLog('getComentarios '+id);
		//$('#comentarios').html('Cargando comentarios...');
		
		//$('#comentarios').show();
		//$('#comentarios').html('<div class="fb-login-button" data-max-rows="1" data-size="medium" data-show-faces="false" data-auto-logout-link="true"></div>');
		
		
		var posting = $.post(Site.intfc,
		{
			action		: 'getComentariosVideo',
			accessToken	: this.accessToken,
			idVideo		: id,
			userID		: Site.userID
		});
		
		posting.done(function(data)
		{
			
			
			//consoleLog(data);
			
			$('#comentarios').html(data);
			$('#comentarios').show();
			
			$('#comentarios').imagesLoaded()
			.always( function( instance )
			{
				FB.getLoginStatus(function(response)
				{
					consoleLog(response.status );
					
					if (response.status === 'connected')
					{
						Site.userID 		= response.authResponse.userID;
						Site.accessToken 	= response.authResponse.accessToken;
						//console.log('Logged in '+Site.accessToken);
						
						Site.saveUserFB();
					}
					else
					{
						consoleLog('else');
						
						$('.comEscribe').html('<div class="loginBtn" data-action="doLoginFB"><img src="assetsW/img/login.png" ></div>');
						Site._attachActionEvents();
					}
				});
				
				$('.textfield').focus(function()
				{
					if ($(this).val()=='Escribe un comentario...')
					{
						$(this).val('')
					}
				});
				
				$('.textfield').blur(function()
				{
					if ($(this).val()=='')
					{
						$(this).val('Escribe un comentario...')
					}
				});
				
				$('.like').each(function ()
				{
					var time = jQuery.timeago($(this).data('time'));
					$(this).html(time);
				});
				
				Site._attachActionEvents();
			});
		});
		
	}
	
	this.startPlayer = function(id)
	{
		consoleLog('Site.videoHash '+Site.videoHash);
		
		if (Site.videoHash != '')
		{
			
			if ($('.youtubeVideo:first').data('param') == Site.videoHash)
			{
				Site.getComentarios(id);
				$('#player').fadeIn();
				$('#player').html('<iframe id="youtube" width="100%" height="100%" src="http://www.youtube.com/embed/'+id+'?enablejsapi=1&wmode=transparent&showinfo=0&autohide=1" frameborder="0" allowfullscreen></iframe>');
				//Site.onYouTubeIframeAPIReady();
				
				Site.videoHash = '';
			}
			else
			{
				Site.carruselNext();
			}
				
			
		}
		else
		{
			Site.getComentarios(id);
			$('#player').fadeIn();
			$('#player').html('<iframe id="youtube" width="100%" height="100%" src="http://www.youtube.com/embed/'+id+'?enablejsapi=1&wmode=transparent&showinfo=0&autohide=1" frameborder="0" allowfullscreen></iframe>');			
			//Site.onYouTubeIframeAPIReady();
		}
		
		
		
		
	}
	
	this.showVideo = function(event)
	{
		var el 	= $(event.delegateTarget);
		var id  = el.data('param');	
		
		Site.startPlayer(id);
	}
	
	/*
	 * Inserta el comentario de la página a la BD
	 */
	this.insertarComentario = function(event)
	{
		var val = $('.textfield').val();
		if (val=='' || val=='Escribe un comentario...')
		{
			alert('Favor de escribir un comentario.');
		}
		else
		{
			var id = $(event.delegateTarget).data("pagina");
			
			var posting = $.post(Site.intfc,
			{
				action: 'insertComentarios',
				accessToken: this.accessToken,
				pagina: id,
				comentario: $('.textfield').val(),
				userID	: Site.userID
			});
			
			posting.done(function(data)
			{
				consoleLog(' insertComentarios RESPONSE');
				consoleLog(data);
				Site.getComentarios(id);
			});	
		}
		
	}
	
	/*
	 * Valida el voto del comentario
	 */
	this.votar = function(event)
	{
		var pagina 	= $(event.delegateTarget).data("pagina");
		
		var posting = $.post(Site.intfc,
		{
			action: 'insertVoto',
			accessToken: this.accessToken,
			pagina: pagina
		});
		
		posting.done(function(data)
		{
			consoleLog(data);
			
			if (data!='false')
			{
				Site.getComentarios(pagina);
			}
			else
			{
				$(event.delegateTarget).append('Ya votaste por este comentario');
			}
			
			
		});
	}
	
	this.onYouTubeIframeAPIReady =function()
	{
		Site.youtubePlayer = new YT.Player('youtube',
		{
			events:
			{
				'onReady': Site.onPlayerReady,
				'onStateChange': Site.onPlayerStateChange
			}
		});
	}
	
	// The API will call this function when the video player is ready.
	this.onPlayerReady = function(event)
	{
		consoleLog('player ready, esperar 1s y play ');
		setTimeout(function()
		{
			if($('#youtube').length>0)
				event.target.playVideo(); 
		},1000);
	}
	
	// The API calls this function when the player's state changes.
	// The function indicates that when playing a video (state=1),
	// the player should play for six seconds and then stop.
	this.onPlayerStateChange = function(event)
	{            
		
	}
		
	this.stopVideo = function()
	{            
	    Site.youtubePlayer.stopVideo();
	}
	
	/*
	 * Inserta el video con un delay del play
	 */
	this.youtubeVideo = function()
	{
		Site.FB.Canvas.scrollTo(0,0);
		
		$('#dynamic').animate(
		{
			left: 1000
		},1000);
		var posting = $.post(Site.intfc,
		{
			action: 'getYoutubeVideo',
			accessToken: this.accessToken
		});
		
		posting.done(function(data)
		{
			$('#dynamic').html(data);
			
			$('.dynamic').imagesLoaded()
			.always( function( instance )
			{
				$('#dynamic').animate(
				{
					left: 0
				},1000);
				Site.onYouTubeIframeAPIReady();
			});
			
			Site._attachActionEvents();			
		});
	}
	
	this.showAviso = function()
	{
		
		$('.terminosTit').html('Aviso Legal');
		$('.terminos').fadeIn();
		$('.terTxt').hide();
		$('.aviTxt').fadeIn();
	}
	
	this.showTerminos = function()
	{
		
		$('.terminosTit').html('Términos y Condiciones');
		$('.terminos').fadeIn();
		$('.aviTxt').hide();
		$('.terTxt').fadeIn();
	}
	
	this.showShare = function()
	{		
		Site.postFB();
	}
	
	this.closeTerminos = function()
	{
		$('.terminos').fadeOut();
	}
	
	/*
	 * Función para generar el post en FB
	 */
	this.postFB = function ()
	{
		
		var picture = 'http://appnet-social.com/iceshot/webshots/assets/img/share.jpg';
		//var picture = 'http://bonobo-projects.com/doubleyou/iceshot/assets/img/share.jpg';
		
		
		consoleLog('video id: '+Site.videos[Site.videosIndice]);
		consoleLog('picture: '+picture);
		
		//doubleyoudemo/app_1440064216248578
		//var url ='https://www.facebook.com/doubleyoudemo/app_1440064216248578?app_data='+Site.videos[Site.videosIndice]
		//var url ='https://www.facebook.com/BonoboDigitalDemo/app_273364706182238?app_data='+Site.videos[Site.videosIndice]
		var url ='http://appnet-social.com/iceshot/webshots?app_data='+Site.videos[Site.videosIndice]
		
		var posting = $.post(Site.intfc,
		{
			action: 'getShortUrl',
			accessToken: this.accessToken,
			url:	url
			
		});
		
		posting.done(function(data)
		{
			consoleLog(data);
			
			var obj =
			{
				method: 	'feed',
				name: 		'Los shots de Ice Shot',
				link: 		data,		
				picture: 	picture,
				caption: 	'Ice Shot',
				description: 	'Ellos pensaban vivir un día normal hasta que se enfrentaron a los Shots de Ice Shot. A veces, sólo un Ice Shot puede salvarte.',
				display:	'dialog'
			};
			
			Site.FB.ui(obj,
			function(response)
			{
				consoleLog('post response');
			});
		});
	}
	
	
	// ------------------------------------------------------------------------------------------
	// ------------------------------------------------------------------------------------------
	// No Modificar los métodos siguientes, son comúnes para todas las apps.
	// ------------------------------------------------------------------------------------------
	// ------------------------------------------------------------------------------------------
		
	/**
	 * Registra los eventos onClick en todos aquellos nodos que tengan definido el atributo 'data-action'
	 *
	 * Además de registrar los eventos toma el valor del atributo para generar la función que se ejecutará en el evento.
	 */
	this._attachActionEvents = function() 
	{
		if(DEBUG) console.log('...Site.attachActionEvents();');
		$('*[data-action]').unbind( "click" );
	
		$('*[data-action]').click(function(event)
		{
			var data 	= $(this).attr( "data-action" );			
			window['Site'][data](event);
		});
	}; // EOF
	
	/**
	 * Devuelve el valor del parámetro sParam en el url
	 */
	this._GetURLParameter = function(sParam)
	{
		if(DEBUG) console.log('...Site.GetURLParameter("' + sParam + '")');
		
		var sPageURL 		= window.location.search.substring(1);
		var sURLVariables 	= sPageURL.split('&');
		
		var resp = '';

		for (var i = 0; i < sURLVariables.length; i++)
		{	
			var sParameterName = sURLVariables[i].split('=');

			if(sParameterName[0] == sParam)
			{
				resp = sParameterName[1];
			}
		}
		
		return resp;
	}; // EOF
	
	this.setHash = function(hash)
	{
		location.hash = hash;
	}
	
	this.getHash = function()
	{
		return location.hash.slice(1);
	}
};

